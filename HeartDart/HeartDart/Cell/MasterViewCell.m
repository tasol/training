//
//  MasterViewCell.m
//  HeartDart
//
//  Created by tasol on 10/31/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import "MasterViewCell.h"

@implementation MasterViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier target:(id)target
{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    parent=target;
    if (self)
    {
        [self setTheme];
    }
    return self;
}

-(void)setTheme
{
    @try
    {
        lblMessage=[[UILabel alloc]initWithFrame:CGRectMake(0, 48, 320, 79)];
        lblMessage.text=@"Message";
        lblMessage.textColor=[UIColor blackColor];
        lblMessage.numberOfLines=4;
        lblMessage.textAlignment=NSTextAlignmentCenter;
        [self addSubview:lblMessage];
        
        lblAuthour=[[UILabel alloc]initWithFrame:CGRectMake(0, 155, 320, 21)];
        lblAuthour.text=@"Author";
        lblAuthour.textColor=[UIColor blackColor];
        lblAuthour.textAlignment=NSTextAlignmentCenter;
        [self addSubview:lblAuthour];
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}


-(void)reload
{
    @try
    {
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}


@end
