//
//  FriendViewController.h
//  HeartDart
//
//  Created by tasol on 11/5/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendViewController : HeartDartViewController<UICollectionViewDataSource,UICollectionViewDelegate,UIAlertViewDelegate>
{
    IBOutlet UICollectionView *collectionViewFriend;
    
    UIView *viewAddFriend;
    UIView *viewAdd;
    IBOutlet UITextField *txtEmailAddress;
    IBOutlet UITextField *txtUserName;
    IBOutlet UIButton *btnSearch;
    NSMutableArray *arrFriend;
    BOOL isFriend;
    IBOutlet UIActivityIndicatorView *spinner;
    IBOutlet UIScrollView *scrollView;
    NSMutableArray *arrFriendRequest;
    UIAlertView *alert;
    
}
@end
