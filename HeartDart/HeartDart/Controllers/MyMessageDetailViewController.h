//
//  MyMessageDetailViewController.h
//  HeartDart
//
//  Created by tasol on 10/30/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyMessageDetailViewController : UIViewController

{
    IBOutlet UILabel *lblUserName;
    IBOutlet UILabel *lblMessageLine;
    IBOutlet UILabel *lblDateTime;
    IBOutlet UIButton *btnSendMessage;
    IBOutlet UIButton *btnMoreInfo;
    IBOutlet UIButton *btnViewProfile;
    IBOutlet UIButton *btnDeleteMessage;
    IBOutlet UIButton *btnReport;
    IBOutlet UIImageView *imgProfile;
    IBOutlet UIImageView *imgMessage;
}
-(IBAction)btnSendMessagePressed:(id)sender;
-(IBAction)btnMoreInfoPressed:(id)sender;
-(IBAction)btnViewProfilePressed:(id)sender;
-(IBAction)btnDeleteMessagePressed:(id)sender;
-(IBAction)btnReportPressed:(id)sender;


@end
