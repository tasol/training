//
//  RegisterViewController.m
//  HeartDart
//
//  Created by tasol on 10/29/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import "RegisterViewController.h"
#import "HeartDartDataProvider.h"
#import "CustomNavigationBar.h"
#import "MyProfileViewController.h"
@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self=[super initWithNibName:[NSString stringWithFormat:@"RegisterViewController%@",[ApplicationConfiguration sharedInstance].POSTFIX_XIB] bundle:nil];
    if (self) {
        // Custom initialization
        userDetails=[[NSDictionary alloc]init];
    }
    return self;

}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTheme];
    [self setNavigationBar];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    NSArray *controller=[self.navigationController viewControllers];
    id lastViewController=[controller objectAtIndex:[controller count]-2];
    if ([lastViewController isKindOfClass:[MyProfileViewController class]])
    {
        [self getProfile];
        
    }
    else
    {
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)setNavigationBar
{
    @try
    {
        CustomNavigationBar *navBar=[[CustomNavigationBar alloc]init];
        
        NSArray *controller=[self.navigationController viewControllers];
        id lastViewController=[controller objectAtIndex:[controller count]-2];
        if ([lastViewController isKindOfClass:[MyProfileViewController class]])
        {
            [navBar setTitle:@"Edit Profile"];
            
            [navBar setLeftSideButtonText:@"Back" withTargate:self action:@selector(btnBackPressed) forEvent:UIControlEventTouchUpInside];
            [navBar setRightSideButtonText:@"Save" withTargate:self action:@selector(updateProfile) forEvent:UIControlEventTouchUpInside];
           
            
        }
        
        else
        {
        [navBar setTitle:@"Register"];
        
        [navBar setLeftSideButtonText:@"Back" withTargate:self action:@selector(btnBackPressed) forEvent:UIControlEventTouchUpInside];
        }
        [self.view addSubview:navBar];
        
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    
}

-(void)btnBackPressed
{
    @try
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}
-(void)setTheme
{
    @try
    {
        UIView *paddingView=[[UIView alloc]initWithFrame:CGRectMake(20, 10, 10, 35)];
        txtEmail.leftView=paddingView;
        txtEmail.leftViewMode=UITextFieldViewModeAlways;
        
        paddingView=[[UIView alloc]initWithFrame:CGRectMake(20, 10, 10, 35)];
        txtUserName.leftView=paddingView;
        txtUserName.leftViewMode=UITextFieldViewModeAlways;
        
        paddingView=[[UIView alloc]initWithFrame:CGRectMake(20, 10, 10, 35)];
        txtPassword.leftView=paddingView;
        txtPassword.leftViewMode=UITextFieldViewModeAlways;
        
        
        paddingView=[[UIView alloc]initWithFrame:CGRectMake(20, 10, 10, 35)];
        txtconfirmPassword.leftView=paddingView;
        txtconfirmPassword.leftViewMode=UITextFieldViewModeAlways;
        
        imgProfile.layer.cornerRadius=(imgProfile.frame.size.width)/2;
        imgProfile.clipsToBounds=YES;
        
        
        NSArray *controller=[self.navigationController viewControllers];
        id lastViewController=[controller objectAtIndex:[controller count]-2];
        if ([lastViewController isKindOfClass:[MyProfileViewController class]])
        {
            lblPassWord.text=@"New Password";
            lblConfirmPassWord.text=@"Confirm New Password";
            btnRegister.hidden=YES;
        }
        
        else
        {
            lblPassWord.text=@"Password";
            lblConfirmPassWord.text=@"Confirm Password";
            btnRegister.hidden=NO;
        }

        
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

#pragma Textview/TextField delegate methods ---------------------------------------------

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    @try {
        [UIView animateWithDuration:1.0f animations:^{
            viewContainer.contentOffset = CGPointMake(0, textField.frame.origin.y-210);
        }];

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    @try
    {
                if(textField == txtEmail)
        {
            [txtUserName becomeFirstResponder];
        }
        else if (textField == txtUserName)
        {
            [txtPassword becomeFirstResponder];
        }
        else if (textField == txtPassword)
        {

            [txtconfirmPassword becomeFirstResponder];
        }
        else if (textField == txtconfirmPassword)
        {
            [textField resignFirstResponder];
            [UIView animateWithDuration:1.0f animations:^{
                viewContainer.contentOffset = CGPointMake(0, 0);
            }];
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    return  YES;
    
}

#pragma Validate Method---------------------------------------------------------------------------

-(BOOL)Validate
{
    @try
    {
        if ([txtEmail.text isEqualToString:@""])
        {
            [super showAlert:@"Email" Content:@"Email Should Not be Blank"];
            [txtEmail becomeFirstResponder];
            return FALSE;
        }
        
        else if([txtUserName.text isEqualToString:@""])
        {
            [super showAlert:@"UserName" Content:@"UserName Should Not be Blank"];
            [txtUserName becomeFirstResponder];
            return FALSE;
        }
        
        else if([txtPassword.text isEqualToString:@""])
        {
            [super showAlert:@"PassWord" Content:@"PassWord Should Not be blank"];
            [txtPassword becomeFirstResponder];
            return FALSE;
        }
        
        else if([txtconfirmPassword.text isEqualToString:@""])
        {
            [super showAlert:@"Confirm PassWord" Content:@"PassWord Should Not be Blank"];
            [txtconfirmPassword becomeFirstResponder];
            return FALSE;
        }
        
        else if(![txtPassword.text isEqualToString:txtconfirmPassword.text])
        {
         
            [super showAlert:@"PassWord" Content:@"PassWord Not Match"];
            return FALSE;
        }
        
        else if (![self validateEmailWithString:txtEmail.text])
        {
            if ([self validateEmailWithString:txtEmail.text]) {
                
            }
            else{
                [super showAlert:@"InValid Email" Content:@"Email Address is not valid"];
                [txtEmail becomeFirstResponder];
                return FALSE;
            }

            
        }
            
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    
    return TRUE;
    
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(IBAction)btnRegisterPressed:(id)sender
{
    @try
    {
        if ([self Validate])
        {
            [spinner startAnimating];
            [self performSelector:@selector(registerUser) withObject:self afterDelay:0.1];
            
        }
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    
}

-(IBAction)btnAddPhotoPressed:(id)sender
{
    @try
    {
        NSString *destructiveTitle = @"Gallery"; //Action Sheet Button Titles
        NSString *other1 = @"Camera";
        NSString *cancelTitle = @"Cancel";
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:destructiveTitle
                                      delegate:self
                                      cancelButtonTitle:cancelTitle
                                      destructiveButtonTitle:destructiveTitle
                                      otherButtonTitles:other1, nil];
        
        [actionSheet showInView:self.view];
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex != 2) {
        [self pickerConfigurations:buttonIndex];
    }
}

- (void)pickerConfigurations:(NSUInteger)buttonIndex
{
    
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    
    picker.delegate=self;
    picker.navigationBar.tintColor=[UIColor blueColor];
    
    if (buttonIndex == 0)
    {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
    }else if (buttonIndex == 1)
    {
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    
    [self presentViewController:picker animated:YES completion:nil];
    
}

#pragma mark -
#pragma mark UIImagepickerview controller delegate methods

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image;
    @try
    {
        [picker dismissViewControllerAnimated: YES completion: nil];
        
        if (picker.sourceType==UIImagePickerControllerSourceTypeCamera)
        {
            image= [info objectForKey:UIImagePickerControllerOriginalImage];
        }
        else
        {
            image = [self scaleAndRotateImage:[info objectForKey:UIImagePickerControllerOriginalImage]];
        }
        
        imgProfile.image = [NSUtil cropImageFromMiddle:image];
        
        imgData = UIImageJPEGRepresentation(image, 1.0f);
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (UIImage *)scaleAndRotateImage:(UIImage *)image {
    //    int kMaxResolution = 800; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}


-(void)registerUser
{
    @try
    {
        NSMutableDictionary *postVariable=[[NSMutableDictionary alloc]init];
        [postVariable setValue:txtEmail.text forKey:@"email"];
        [postVariable setValue:txtUserName.text forKey:@"username"];
        [postVariable setValue:txtPassword.text forKey:@"password"];
        [postVariable setValue:[NSString stringWithFormat:@"%f",[ApplicationConfiguration sharedInstance].locationManager.latitude] forKey:@"latitude"];
        [postVariable setValue:[NSString stringWithFormat:@"%f",[ApplicationConfiguration sharedInstance].locationManager.longitude] forKey:@"longitude"];
        BOOL success=[[HeartDartDataProvider sharedInstance]registerUserWithTaskData:postVariable imageData:imgData ];
        if (success)
        {
            [self showAlert:@"Registration Completed" Content:@"We have send you a email with a link to activate your Account"];
            [self.navigationController popViewControllerAnimated:YES];
            
        }
        
        
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        [spinner stopAnimating];
        
    }
}



-(void)updateProfile
{
    @try
    {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setValue:txtEmail.text forKey:@"email"];
        [dict setValue:txtUserName.text forKey:@"userName"];
        [dict setValue:txtPassword.text forKey:@"password"];
        
        BOOL success=[[HeartDartDataProvider sharedInstance] updatedProfile:dict];
        if (success)
        {
            [super showAlert:@"Updated Successfully" Content:@"User detail are updated successfully"];
        }
        
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(void)getProfile
{
    @try
    {
        NSMutableDictionary *postVariable=[[NSMutableDictionary alloc]init ];
        [postVariable setValue:[ApplicationConfiguration sharedInstance].restID forKey:@"userID"];
        userDetails= [[HeartDartDataProvider sharedInstance]getProfile:postVariable];
        if (!(userDetails==nil))
        {
            [self setValue];
            
        }
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(void)setValue
{
    @try
    {
        imgProfile.image=[NSUtil cropImageFromMiddle:[NSUtil getImageFromURL:[userDetails valueForKey:@"avatar"]]];
        txtEmail.text=[userDetails valueForKey:@"email"];
        txtUserName.text=[userDetails valueForKey:@"username"];
        
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    
}

@end
