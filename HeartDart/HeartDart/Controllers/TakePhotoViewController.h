//
//  TakePhotoViewController.h
//  HeartDart
//
//  Created by tasol on 10/31/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TakePhotoViewController : UIViewController

{
    IBOutlet UIButton *btnAddPhoto;
    IBOutlet UIButton *btnTakeCamera;
    IBOutlet UIImageView *imgTakeCamera;
}

-(IBAction)btnAddPhotoPressed:(id)sender;
-(IBAction)btnTakeCameraPressed:(id)sender;
@end
