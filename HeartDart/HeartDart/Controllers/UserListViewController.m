//
//  FriendViewController.m
//  HeartDart
//
//  Created by tasol on 11/4/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import "UserListViewController.h"
#import "CustomNavigationBar.h"
#import "AppDelegate.h"
#import "UserListCell.h"
#import "HeartDartDataProvider.h"
#import "FriendDetailViewController.h"
@interface UserListViewController ()

@end

@implementation UserListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        arrFriend=[[NSArray alloc]init];
        arrFriend=[[NSMutableArray alloc]init];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [collectionViewFriend registerClass:[UserListCell class] forCellWithReuseIdentifier:@"UserListCell"];
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.btnSideMenu.hidden=YES;
    
    [self setNavigationBar];
}

-(void)viewWillAppear:(BOOL)animated
{
    [spinner startAnimating];
    [self performSelector:@selector(getFriend) withObject:self afterDelay:1.0];
    [self setTheme];
}

-(void)setNavigationBar
{
    @try
    {
        CustomNavigationBar *navBar=[[CustomNavigationBar alloc]init];
        [navBar setTitle:@"Add Friend"];
        [navBar setRightSideButtonText:@"Search" withTargate:self action:@selector(btnAddPressed) forEvent:UIControlEventTouchUpInside];
        [navBar setLeftSideButtonText:@"Back" withTargate:self action:@selector(btnBackPressed) forEvent:UIControlEventTouchUpInside];
        [self.view addSubview:navBar];
        
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}
-(void)btnBackPressed
{
    @try
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}
-(void)setTheme
{
    @try
    {
        UIView *paddingView=[[UIView alloc]initWithFrame:CGRectMake(20, 10, 10, 35)];
        txtUserName.leftView=paddingView;
        txtUserName.leftViewMode=UITextFieldViewModeAlways;
        txtUserName=[[UITextField alloc]initWithFrame:CGRectMake(15,88, 293, 32)];
        txtUserName.background=[UIImage imageNamed:@"textbox.png"];
        txtUserName.placeholder=@"Username";
        
        paddingView=[[UIView alloc]initWithFrame:CGRectMake(20, 10, 10, 35)];
        txtEmailAddress.leftView=paddingView;
        txtEmailAddress.leftViewMode=UITextFieldViewModeAlways;
        txtEmailAddress=[[UITextField alloc]initWithFrame:CGRectMake(15,146, 293, 32)];
        txtEmailAddress.background=[UIImage imageNamed:@"textbox.png"];
        txtEmailAddress.placeholder=@"Email Address";
        
        btnSearch=[[UIButton alloc]initWithFrame:CGRectMake(0, 200, 320, 50)];
        [btnSearch setImage:[UIImage imageNamed:@"btn_search_off.png"] forState:UIControlStateNormal];
        
//        collectionViewFriend.
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}
-(void)btnAddPressed
{
    @try
    {
        viewAddFriend=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)];
        viewAddFriend.backgroundColor=[UIColor clearColor];
        
        [self.view addSubview:viewAddFriend];
        
        viewAdd=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 257)];
        viewAdd.backgroundColor=[UIColor whiteColor];
        [viewAddFriend addSubview:viewAdd];
       
        [viewAdd addSubview:txtUserName];
        
        [viewAdd addSubview:txtEmailAddress];
        
        [viewAdd addSubview:btnSearch];
        
        [self setNavigationBarAddFriend];
        
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(void)setNavigationBarAddFriend
{
    @try
    {
        CustomNavigationBar *navBar=[[CustomNavigationBar alloc]init];
        [navBar setTitle:@"Add Friend"];
        [navBar setRightSideButtonText:@"Close" withTargate:self action:@selector(btnClosePressed) forEvent:UIControlEventTouchUpInside];
        
        [viewAdd addSubview:navBar];
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(void)btnClosePressed
{
    @try
    {
//        [self.view removeFromSuperview];
        [viewAddFriend setHidden:YES];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
    }
}
-(void)getFriend
{
    @try
    {
        isFriend=0;
        
        NSMutableDictionary *postVariable=[[NSMutableDictionary alloc]init];
        [postVariable setValue:[NSString stringWithFormat:@"%d",isFriend] forKey:@"friendsOnly"];
        arrFriend=[[HeartDartDataProvider sharedInstance]getUserList:postVariable ];
        [collectionViewFriend reloadData];
}
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        [spinner stopAnimating];
        
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseidentifier = @"UserListCell";
    UserListCell *cell;
    @try
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseidentifier forIndexPath:indexPath];
        cell.layer.shouldRasterize = YES;
        cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
        
        cell.userDetails = [arrFriend objectAtIndex:indexPath.row];
        [cell reloadCell];
       
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
     return cell;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [arrFriend count];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(92, 92);
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 10, 10, 10);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
        FriendDetailViewController *friendDetail=[[FriendDetailViewController alloc]init];
        [self.navigationController pushViewController:friendDetail animated:YES];
        friendDetail.FriendDetail=[arrFriend objectAtIndex:indexPath.row];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    
    
}
@end
