//
//  YouTubeVideoViewController.h
//  HeartDart
//
//  Created by tasol on 10/31/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YouTubeVideoViewController : UIViewController<UITextFieldDelegate>

{
    IBOutlet UIButton *btnAddVideo;
    IBOutlet UITextField *txtYouTubeURL;
    
}

-(IBAction)btnAddVideoPressed:(id)sender;

@end
