//
//  RevOnBoardingViewController.m
//  iJoomer
//
//  Created by tasol on 3/20/14.
//
//

#import "HeartDartOnBoardingViewController.h"
#import "RegisterViewController.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import <CoreImage/CoreImage.h>
#import "CustomNavigationBar.h"
//#import "ApplicationData.h"
//#import "RevTakePhotoViewController.h"
//#import "RevAllReveelsViewController.h"

@interface HeartDartOnBoardingViewController ()

@end

@implementation HeartDartOnBoardingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    spinner.layer.cornerRadius = 8;
    spinner.layer.masksToBounds = YES;
    [self startAnimating];
    [self setNavigationBar];
    [self performSelector:@selector(setFrame) withObject:nil afterDelay:1.0];
//    [self updateDots];
    
    btnLogin.hidden = YES;
    pageControl.hidden = YES;
    
    lblStatusBar.hidden = YES;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        
        if(result.height == 480)
        {
            spinner.frame = CGRectMake((self.view.frame.size.width-37)/2, (480-37)/2, 37, 37);
        }
        else  if(result.height == 568)
        {
            spinner.frame = CGRectMake((self.view.frame.size.width-37)/2, (568-37)/2, 37, 37);
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
//    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
//    appDelegate.TabView.hidden = YES;
//    appDelegate.BtnSideMenu.hidden = YES;
//    self.navigationController.navigationBarHidden = NO;
    
//    if([ApplicationData sharedInstance].isLoggedIn)
//    {
//        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
//        
//        RevAllReveelsViewController *controller = [[RevAllReveelsViewController alloc] init];
//        [self.navigationController pushViewController:controller animated:NO];
//        //[self.navigationController popViewControllerAnimated:NO];
//    }
}

-(void)setNavigationBar
{
    @try
    {
        CustomNavigationBar *navigationBar=[[CustomNavigationBar alloc]init];
        [navigationBar setTitle:@"Heart Dart"];
        
        [navigationBar setRightSideButtonText:@"Login" withTargate:self action:@selector(btnLoginPressed:) forEvent:UIControlEventTouchUpInside];
        [self.view addSubview:navigationBar];
    
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}
-(void)startAnimating
{
    [self.view bringSubviewToFront:spinner];
    self.view.userInteractionEnabled = NO;
    [spinner startAnimating];
}

-(void)stopAnimating
{
    self.view.userInteractionEnabled = YES;
    [spinner stopAnimating];
}

-(void)setFrame
{
    btnLogin.frame = CGRectMake(260, 20, 60, 30);
    
    scrolView = [[UIScrollView alloc] init];
    scrolView.pagingEnabled = YES;
    scrolView.scrollEnabled = YES;
    scrolView.delegate = self;
    scrolView.frame = CGRectMake(0, 50, 320, self.view.frame.size.height-50);
    scrolView.contentSize = CGSizeMake(320*3, scrolView.frame.size.height);
    [self.view addSubview:scrolView];
    
    [self.view bringSubviewToFront:pageControl];
    
    [self setFrameForPage1];
    [self setFrameForPage2];
    [self setFrameForPage3];
    
    btnLogin.hidden = NO;
    pageControl.hidden = NO;
    [self stopAnimating];
    
    [self.view bringSubviewToFront:lblStatusBar];
    
    [self performSelector:@selector(showAnimationImage1) withObject:nil afterDelay:3.0];
}

-(void)setFrameForPage1
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        
        if(result.height == 480)
        {
            float yPoint = -8;
            
            lblBoardingTop1 = [[UILabel alloc] init];
            lblBoardingTop1.text = @"or browse 1000s of messgaes in our public library";
            lblBoardingTop1.backgroundColor = [UIColor clearColor];
            lblBoardingTop1.textAlignment = NSTextAlignmentCenter;
            lblBoardingTop1.textColor = [UIColor whiteColor];
            lblBoardingTop1.font = [UIFont systemFontOfSize:18.0];
            lblBoardingTop1.numberOfLines = 3;
            lblBoardingTop1.frame = CGRectMake(50, yPoint+10, 220, 80);
            [scrolView addSubview:lblBoardingTop1];
//            [lblBoardingTop1 release];
            
            yPoint = yPoint + lblBoardingTop1.frame.size.height + 50;
            
            imgBoardind1 = [[UIImageView alloc] init];
            imgBoardind1.image = [UIImage imageNamed:@"On Boarding 1.png"];
            imgBoardind1.frame = CGRectMake(85, 250, 150, 280);
            [scrolView addSubview:imgBoardind1];
//            [imgBoardind1 release];
            
            yPoint = yPoint + imgBoardind1.frame.size.height-5;
            
            lblBoardingBottom1 = [[UILabel alloc] init];
            lblBoardingBottom1.text = @"Add an enticing caption";
            lblBoardingBottom1.backgroundColor = [UIColor clearColor];
            lblBoardingBottom1.textAlignment = NSTextAlignmentCenter;
            lblBoardingBottom1.textColor = [UIColor whiteColor];
            lblBoardingBottom1.font = [UIFont systemFontOfSize:16.0];
            lblBoardingBottom1.frame = CGRectMake(50, yPoint, 220, 50);
//            [scrolView addSubview:lblBoardingBottom1];
//            [lblBoardingBottom1 release];
            
            yPoint = yPoint + lblBoardingBottom1.frame.size.height;
            
            pageControl.frame = CGRectMake(125, scrolView.frame.size.height-30, 70, 37);
            yPoint = yPoint + 10;
            
            UIImageView *imgSeperator1 = [[UIImageView alloc] init];
            imgSeperator1.backgroundColor = [UIColor whiteColor];
            imgSeperator1.frame = CGRectMake(50, scrolView.frame.size.height-45, 220 , 1);
            [scrolView addSubview:imgSeperator1];
//            [imgSeperator1 release];
            
            UIButton *btnCreate = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnCreate setTitle:@"Create Account" forState:UIControlStateNormal];
            [btnCreate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnCreate setBackgroundColor:[UIColor clearColor]];
            [btnCreate addTarget:self action:@selector(btnCreateAccountPressed:) forControlEvents:UIControlEventTouchUpInside];
            btnCreate.frame = CGRectMake(50, scrolView.frame.size.height-45, 220, 50);
            [scrolView addSubview:btnCreate];

        }
        else  if(result.height == 568)
        {
            float yPoint = 0;
            
            lblBoardingTop1 = [[UILabel alloc] init];
            lblBoardingTop1.text = @"or browse 1000s of messgaes in our public library";
            lblBoardingTop1.backgroundColor = [UIColor clearColor];
            lblBoardingTop1.textAlignment = NSTextAlignmentCenter;
            lblBoardingTop1.textColor = [UIColor whiteColor];
            lblBoardingTop1.font = [UIFont systemFontOfSize:18.0];
            lblBoardingTop1.numberOfLines = 3;
            lblBoardingTop1.frame = CGRectMake(50, yPoint+10, 220, 80);
            [scrolView addSubview:lblBoardingTop1];
//            [lblBoardingTop1 release];
            
            yPoint = yPoint + lblBoardingTop1.frame.size.height + 10;
            
            imgBoardind1 = [[UIImageView alloc] init];
            imgBoardind1.image = [UIImage imageNamed:@"On Boarding 1.png"];
            imgBoardind1.frame = CGRectMake(60, yPoint+50, 200, 340);
            [scrolView addSubview:imgBoardind1];
//            [imgBoardind1 release];
            
            yPoint = yPoint + imgBoardind1.frame.size.height;
            
//            lblBoardingBottom1 = [[UILabel alloc] init];
//            lblBoardingBottom1.text = @"Add an enticing caption";
//            lblBoardingBottom1.backgroundColor = [UIColor clearColor];
//            lblBoardingBottom1.textAlignment = NSTextAlignmentCenter;
//            lblBoardingBottom1.textColor = [UIColor whiteColor];
//            lblBoardingBottom1.font = [UIFont systemFontOfSize:16.0];
//            lblBoardingBottom1.frame = CGRectMake(50, yPoint, 220, 50);
//            [scrolView addSubview:lblBoardingBottom1];
//            [lblBoardingBottom1 release];
//            
//            yPoint = yPoint + lblBoardingBottom1.frame.size.height + 30;
            
            pageControl.frame = CGRectMake(125, scrolView.frame.size.height-35, 70, 37);
            
            yPoint = yPoint + 10;
            
            UIImageView *imgSeperator1 = [[UIImageView alloc] init];
            imgSeperator1.backgroundColor = [UIColor whiteColor];
            imgSeperator1.frame = CGRectMake(50, scrolView.frame.size.height-50, 220 , 1);
            [scrolView addSubview:imgSeperator1];
//            [imgSeperator1 release];
            
            UIButton *btnCreate = [UIButton buttonWithType:UIButtonTypeCustom];
//            [btnCreate setTitle:@"Create Account" forState:UIControlStateNormal];
//            [btnCreate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnCreate setBackgroundImage:[UIImage imageNamed:@"btn_register_off.png"] forState:UIControlStateNormal];
            [btnCreate addTarget:self action:@selector(btnCreateAccountPressed:) forControlEvents:UIControlEventTouchUpInside];
            btnCreate.frame = CGRectMake(0, scrolView.frame.size.height-50, 320, 53);
            [scrolView addSubview:btnCreate];
        }
    }
}

-(void)setFrameForPage2
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        
        if(result.height == 480)
        {
            float yPoint = -8;
            float xPoint = 320*1;

            UILabel *lblTopPage1 = [[UILabel alloc] init];
            lblTopPage1.text = @"Users can see your post with its hidden item";
            lblTopPage1.backgroundColor = [UIColor clearColor];
            lblTopPage1.textAlignment = NSTextAlignmentCenter;
            lblTopPage1.textColor = [UIColor whiteColor];
            lblTopPage1.font = [UIFont systemFontOfSize:18.0];
            lblTopPage1.numberOfLines = 2;
            lblTopPage1.frame = CGRectMake(xPoint+50, yPoint, 220, 50);
            [scrolView addSubview:lblTopPage1];
//            [lblTopPage1 release];

            yPoint = yPoint + lblTopPage1.frame.size.height + 5;
            
            imgBoardind2 = [[UIImageView alloc] init];
            imgBoardind2.image = [UIImage imageNamed:@"On Boarding 2.png"];
            imgBoardind2.frame = CGRectMake(xPoint+85, yPoint, 150, 280);
            [scrolView addSubview:imgBoardind2];
//            [imgBoardind2 release];
            
            yPoint = yPoint + imgBoardind1.frame.size.height-5;
            
            UILabel *lblBottomPage1 = [[UILabel alloc] init];
            lblBottomPage1.text = @"Then can request you to reveal all of your post and where you are";
            lblBottomPage1.backgroundColor = [UIColor clearColor];
            lblBottomPage1.textAlignment = NSTextAlignmentCenter;
            lblBottomPage1.textColor = [UIColor whiteColor];
            lblBottomPage1.font = [UIFont systemFontOfSize:16.0];
            lblBottomPage1.numberOfLines = 2;
            lblBottomPage1.frame = CGRectMake(xPoint+30, yPoint, 260, 50);
            [scrolView addSubview:lblBottomPage1];
//            [lblBottomPage1 release];
            
            yPoint = yPoint + lblBottomPage1.frame.size.height;
            
            pageControl.frame = CGRectMake(125, scrolView.frame.size.height-30, 70, 37);
            
            yPoint = yPoint + 10;
            
            UIImageView *imgSeperator1 = [[UIImageView alloc] init];
            imgSeperator1.backgroundColor = [UIColor whiteColor];
            imgSeperator1.frame = CGRectMake(xPoint+50, scrolView.frame.size.height-45, 220 , 1);
            [scrolView addSubview:imgSeperator1];
//            [imgSeperator1 release];
            
            UIButton *btnCreate = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnCreate setTitle:@"Create Account" forState:UIControlStateNormal];
            [btnCreate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnCreate setBackgroundColor:[UIColor clearColor]];
            [btnCreate addTarget:self action:@selector(btnCreateAccountPressed:) forControlEvents:UIControlEventTouchUpInside];
            btnCreate.frame = CGRectMake(xPoint+50, scrolView.frame.size.height-45, 220, 50);
            [scrolView addSubview:btnCreate];
        }
        else  if(result.height == 568)
        {
            float yPoint = 0;
            float xPoint = 320*1;
            
            UILabel *lblTopPage1 = [[UILabel alloc] init];
            lblTopPage1.text = @"Users can see your post with its hidden item";
            lblTopPage1.backgroundColor = [UIColor clearColor];
            lblTopPage1.textAlignment = NSTextAlignmentCenter;
            lblTopPage1.textColor = [UIColor whiteColor];
            lblTopPage1.font = [UIFont systemFontOfSize:18.0];
            lblTopPage1.numberOfLines = 2;
            lblTopPage1.frame = CGRectMake(xPoint+50, yPoint, 220, 50);
            [scrolView addSubview:lblTopPage1];
//            [lblTopPage1 release];
            
            yPoint = yPoint + lblTopPage1.frame.size.height + 10;
            
            imgBoardind2 = [[UIImageView alloc] init];
            imgBoardind2.image = [UIImage imageNamed:@"On Boarding 2.png"];
            imgBoardind2.frame = CGRectMake(xPoint+80, yPoint, 160, 340);
            [scrolView addSubview:imgBoardind2];
//            [imgBoardind2 release];
            
            yPoint = yPoint + imgBoardind2.frame.size.height;
            
            UILabel *lblBottomPage1 = [[UILabel alloc] init];
            lblBottomPage1.text = @"Then can request you to reveal all of your post and where you are";
            lblBottomPage1.backgroundColor = [UIColor clearColor];
            lblBottomPage1.textAlignment = NSTextAlignmentCenter;
            lblBottomPage1.textColor = [UIColor whiteColor];
            lblBottomPage1.font = [UIFont systemFontOfSize:16.0];
            lblBottomPage1.numberOfLines = 2;
            lblBottomPage1.frame = CGRectMake(xPoint+30, yPoint, 260, 50);
            [scrolView addSubview:lblBottomPage1];
//            [lblBottomPage1 release];
            
            yPoint = yPoint + lblBottomPage1.frame.size.height + 30;
            
            pageControl.frame = CGRectMake(125, scrolView.frame.size.height-35, 70, 37);
            yPoint = yPoint + 10;
            
            UIImageView *imgSeperator1 = [[UIImageView alloc] init];
            imgSeperator1.backgroundColor = [UIColor whiteColor];
            imgSeperator1.frame = CGRectMake(xPoint+50, scrolView.frame.size.height-50, 220 , 1);
            [scrolView addSubview:imgSeperator1];
//            [imgSeperator1 release];
            
            UIButton *btnCreate = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnCreate setTitle:@"Create Account" forState:UIControlStateNormal];
            [btnCreate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnCreate setBackgroundColor:[UIColor clearColor]];
            [btnCreate addTarget:self action:@selector(btnCreateAccountPressed:) forControlEvents:UIControlEventTouchUpInside];
            btnCreate.frame = CGRectMake(xPoint+50, scrolView.frame.size.height-50, 220, 50);
            [scrolView addSubview:btnCreate];
        }
    }
}

-(void)setFrameForPage3
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        
        if(result.height == 480)
        {
            float yPoint = -8;
            float xPoint = 320*2;
            
            UILabel *lblTopPage1 = [[UILabel alloc] init];
            lblTopPage1.text = @"If you like what you see, approve their request";
            lblTopPage1.backgroundColor = [UIColor clearColor];
            lblTopPage1.textAlignment = NSTextAlignmentCenter;
            lblTopPage1.textColor = [UIColor whiteColor];
            lblTopPage1.font = [UIFont systemFontOfSize:18.0];
            lblTopPage1.numberOfLines = 2;
            lblTopPage1.frame = CGRectMake(xPoint+50, yPoint, 220, 50);
            [scrolView addSubview:lblTopPage1];
//            [lblTopPage1 release];
            
            yPoint = yPoint + lblTopPage1.frame.size.height + 5;
            
            imgBoardind3 = [[UIImageView alloc] init];
            imgBoardind3.image = [UIImage imageNamed:@"On Boarding 3.png"];
           // imgBoardind1.image = [[ApplicationData sharedInstance] blur:[UIImage imageNamed:@"On Boarding 3.png"]];
            imgBoardind3.frame = CGRectMake(xPoint+85, yPoint, 150, 280);
            [scrolView addSubview:imgBoardind3];
//            [imgBoardind3 release];
            
            yPoint = yPoint + imgBoardind1.frame.size.height-5;
            
            UILabel *lblBottomPage1 = [[UILabel alloc] init];
            lblBottomPage1.text = @"The rest is up to you";
            lblBottomPage1.backgroundColor = [UIColor clearColor];
            lblBottomPage1.textAlignment = NSTextAlignmentCenter;
            lblBottomPage1.textColor = [UIColor whiteColor];
            lblBottomPage1.font = [UIFont systemFontOfSize:16.0];
            lblBottomPage1.numberOfLines = 2;
            lblBottomPage1.frame = CGRectMake(xPoint+50, yPoint, 220, 50);
            [scrolView addSubview:lblBottomPage1];
//            [lblBottomPage1 release];
            
            yPoint = yPoint + lblBottomPage1.frame.size.height;
            
            pageControl.frame = CGRectMake(125, scrolView.frame.size.height-30, 70, 37);
            yPoint = yPoint + 10;
            
            UIImageView *imgSeperator1 = [[UIImageView alloc] init];
            imgSeperator1.backgroundColor = [UIColor whiteColor];
            imgSeperator1.frame = CGRectMake(xPoint+50, scrolView.frame.size.height-45, 220 , 1);
            [scrolView addSubview:imgSeperator1];
//            [imgSeperator1 release];
            
            UIButton *btnCreate = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnCreate setTitle:@"Create A Post" forState:UIControlStateNormal];
            [btnCreate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnCreate setBackgroundColor:[UIColor clearColor]];
            [btnCreate addTarget:self action:@selector(btnCreateReveelPressed:) forControlEvents:UIControlEventTouchUpInside];
            btnCreate.frame = CGRectMake(xPoint+50, scrolView.frame.size.height-45, 220, 50);
            [scrolView addSubview:btnCreate];
        }
        else  if(result.height == 568)
        {
            float yPoint = 0;
            float xPoint = 320*2;
            
            UILabel *lblTopPage1 = [[UILabel alloc] init];
            lblTopPage1.text = @"If you like what you see, approve their request";
            lblTopPage1.backgroundColor = [UIColor clearColor];
            lblTopPage1.textAlignment = NSTextAlignmentCenter;
            lblTopPage1.textColor = [UIColor whiteColor];
            lblTopPage1.font = [UIFont systemFontOfSize:18.0];
            lblTopPage1.numberOfLines = 2;
            lblTopPage1.frame = CGRectMake(xPoint+50, yPoint, 220, 50);
            [scrolView addSubview:lblTopPage1];
//            [lblTopPage1 release];
            
            yPoint = yPoint + lblTopPage1.frame.size.height + 10;
            
            imgBoardind3 = [[UIImageView alloc] init];
            imgBoardind3.image = [UIImage imageNamed:@"On Boarding 3.png"];
            imgBoardind3.frame = CGRectMake(xPoint+80, yPoint, 160, 340);
            [scrolView addSubview:imgBoardind3];
//            [imgBoardind3 release];
            
            yPoint = yPoint + imgBoardind2.frame.size.height;
            
            UILabel *lblBottomPage1 = [[UILabel alloc] init];
            lblBottomPage1.text = @"The rest is up to you";
            lblBottomPage1.backgroundColor = [UIColor clearColor];
            lblBottomPage1.textAlignment = NSTextAlignmentCenter;
            lblBottomPage1.textColor = [UIColor whiteColor];
            lblBottomPage1.font = [UIFont systemFontOfSize:16.0];
            lblBottomPage1.numberOfLines = 2;
            lblBottomPage1.frame = CGRectMake(xPoint+50, yPoint, 220, 50);
            [scrolView addSubview:lblBottomPage1];
//            [lblBottomPage1 release];
            
            yPoint = yPoint + lblBottomPage1.frame.size.height + 30;
            
            pageControl.frame = CGRectMake(125, scrolView.frame.size.height-35, 70, 37);
            yPoint = yPoint + 10;
            
            UIImageView *imgSeperator1 = [[UIImageView alloc] init];
            imgSeperator1.backgroundColor = [UIColor whiteColor];
            imgSeperator1.frame = CGRectMake(xPoint+50, scrolView.frame.size.height-50, 220 , 1);
            [scrolView addSubview:imgSeperator1];
//            [imgSeperator1 release];
            
            UIButton *btnCreate = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnCreate setTitle:@"Create A Post" forState:UIControlStateNormal];
            [btnCreate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnCreate setBackgroundColor:[UIColor clearColor]];
            [btnCreate addTarget:self action:@selector(btnCreateReveelPressed:) forControlEvents:UIControlEventTouchUpInside];
            btnCreate.frame = CGRectMake(xPoint+50, scrolView.frame.size.height-50, 220, 50);
            [scrolView addSubview:btnCreate];
        }
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x == 0)
    {
        pageControl.currentPage = 0;
        
        imgBoardind2.image = [UIImage imageNamed:@"On Boarding 2.png"];
        imgBoardind3.image = [UIImage imageNamed:@"On Boarding 3.png"];
        
        [self performSelector:@selector(showAnimationImage1) withObject:nil afterDelay:3.0];
    }
    else if (scrollView.contentOffset.x == 320)
    {
        pageControl.currentPage = 1;
        
        imgBoardind1.image = [UIImage imageNamed:@"On Boarding 1.png"];
        imgBoardind3.image = [UIImage imageNamed:@"On Boarding 3.png"];
        lblBoardingTop1.text = @"or browse 1000s of messgaes in our public library";
//        lblBoardingBottom1.text = @"Add an enticing caption";

       // [self performSelector:@selector(showAnimationImage2) withObject:nil afterDelay:3.0];
    }
    else if (scrollView.contentOffset.x == 640)
    {
        pageControl.currentPage = 2;
        
        imgBoardind1.image = [UIImage imageNamed:@"On Boarding 1a.png"];
        imgBoardind2.image = [UIImage imageNamed:@"On Boarding 2.png"];
        lblBoardingTop1.text = @"Take a photo or video of who and where you are";
        lblBoardingBottom1.text = @"Add an enticing caption";
        
        //[self performSelector:@selector(showAnimationImage3) withObject:nil afterDelay:3.0];
    }
}

-(IBAction)changePage:(id)sender
{
    if (pageControl.currentPage == 0)
    {
        scrolView.contentOffset = CGPointMake(0, 0);
    }
    else if (pageControl.currentPage == 1)
    {
        scrolView.contentOffset = CGPointMake(320, 0);
    }
    else if (pageControl.currentPage == 2)
    {
        scrolView.contentOffset = CGPointMake(640, 0);
    }
}

-(void)showAnimationImage1
{
    imgBoardind1.image = [UIImage imageNamed:@"On Boarding 1b.png"];
    imgBoardind1.alpha = 1.0;
    
    lblBoardingTop1.text = @"Select something to hide, to keep them guessing";
    lblBoardingTop1.alpha = 1.0;
    
    lblBoardingBottom1.text = @"Your location will be hidden";
    lblBoardingBottom1.alpha = 1.0;
    
    CATransition *anim = [CATransition animation];
    [anim setDuration:4.0];
    [anim setType:kCATransitionFade];
    [anim setSubtype:kCATransition];
    [anim setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    
    [[imgBoardind1 layer] addAnimation:anim forKey:@"fade in"];
    [[lblBoardingTop1 layer] addAnimation:anim forKey:@"fade in"];
    [[lblBoardingBottom1 layer] addAnimation:anim forKey:@"fade in"];
}

-(void)showAnimationImage2
{
    imgBoardind2.image = [UIImage imageNamed:@"On Boarding 5.png"];
    imgBoardind2.alpha = 1.0;
    
    CATransition *anim = [CATransition animation];
    [anim setDuration:4.0];
    [anim setType:kCATransitionFade];
    [anim setSubtype:kCATransition];
    [anim setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    
    [[imgBoardind2 layer] addAnimation:anim forKey:@"fade in"];
}

-(void)showAnimationImage3
{
    imgBoardind3.image = [UIImage imageNamed:@"On Boarding 4.png"];
    imgBoardind3.alpha = 1.0;
    
    CATransition *anim = [CATransition animation];
    [anim setDuration:4.0];
    [anim setType:kCATransitionFade];
    [anim setSubtype:kCATransition];
    [anim setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    
    [[imgBoardind3 layer] addAnimation:anim forKey:@"fade in"];
}

-(IBAction)btnCreateAccountPressed:(id)sender
{
    RegisterViewController *controller = [[RegisterViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
//    [ApplicationData sharedInstance].isCreateReveelFromMyReveel = NO;
//    [controller release];
}

-(IBAction)btnCreateReveelPressed:(id)sender
{
//    RevTakePhotoViewController *controller = [[RevTakePhotoViewController alloc] init];
//    [self.navigationController pushViewController:controller animated:YES];
////    [controller release];
}

-(IBAction)btnLoginPressed:(id)sender
{
    LoginViewController *controller = [[LoginViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
//    [self navigationController:controller animated:YES completion:nil];
//    [controller release];
}


@end
