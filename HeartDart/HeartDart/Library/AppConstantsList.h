//
//  Created by Tailored Solutions on 03/07/12.
//  Copyright 2013 Tailored Solutions. All rights reserved.
//
#import <Foundation/Foundation.h>

//////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////  Constants /////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//#ifdef DEBUG
#define DLog( s, ... ) NSLog( @"<%p %@:(%d)> %@", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
//#define DLog( s, ... ) 
//#else
//#define DLog( s, ... )
//#endif
////Database Table
#define TABLE_THEME @"theme"

////Querys To CreateDataBase
#define CREATE_THEME @"CREATE TABLE IF NOT EXISTS \"theme\" (\"viewname\" TEXT PRIMARY KEY,\"tab\" TEXT,\"tab_active\" TEXT,\"home\" TEXT);"
#define CREATE_MENUS @"CREATE TABLE IF NOT EXISTS \"menus\" (\"menuid\" TEXT PRIMARY KEY,\"menuname\" TEXT,\"menuposition\" TEXT,\"screens\" TEXT);"
#define CREATE_MENU_ITEMS @"CREATE TABLE IF NOT EXISTS \"menuitem\" (\"itemid\" TEXT PRIMARY KEY,\"itemcaption\" TEXT,\"itemview\" TEXT,\"itemdata\" TEXT);"


#define TABLE_CELL_HEIGHT			68
#define UPLOAD_IMAGE_MAX_HEIGHT		200
#define UPLOAD_IMAGE_MAX_WIDTH		200
#define VIEW_TYPE                   @"thumb"


#define LOCATION_WAIT_TIME			15// in seconds
#define CORNER_RADIUS				15
#define SEARCH_INCREMENT			10
#define MYTOPIC_LIST_LIMIT          20
#define LIST_LIMIT					20
#define PAGE_COUNT					16
#define BLOG_PAGE_LIMIT				10
#define EVENT_LIST_LIMIT			10
#define BOTTOM_TAB_ITEM_COUNT       5
#define TAB_COUNT					9
#define MSG_LIST_LIMIT              15
#define ALBUM_LIMIT                 12
#define KNoId                       -999
#define SPACE_BETWEEN_TABBAR_BUTTON 18.33f
#define SPACE_BETWEEN_TABBAR_BUTTON_IPad 93.0
#define TABBAR_BUTTON_WIDTH 40.0f


#define MIN_DISTANCE                1
#define MAX_DISTANCE                100
#define kDistanceKey                @"setdistance"

#define USE_SERVER_TABS             YES
#define ISCHATREQUIRED              NO
#define ISARTICLEADDENABLED         YES
#define ISEDITORIMAGEREQUIRED		YES
#define ISFACEBOOKREQUIRED          YES
#define FIELD_PICKER                @"select"
#define FIELD_SWITCH                @"radio"
#define RADIOBUTTON_ON              @"1"
#define RADIOBUTTON_OFF             @"0"
#define ISUPDATEVOICE                YES 
#define ISPROFILEEVOICE              YES
#define ISCOMMENTVOICE               YES
#define ISPHOTOVOICE                 YES 
#define ISPHOTOCOMMENTVOICE          YES 
#define ISVIDEOCOMMENTVOICE          YES 
#define ISGROUPCOMMENTVOICE          YES 
#define ISGROUPWALLCOMMENTVOICE      YES 
#define ISEVENTDETAILTVOICE          YES
#define ISREPLYLISTTVOICE            YES
#define ISWRITEMSGTVOICE             YES
#define ISGROUPCHATVOICE              YES

// For Application Type
#define APPLICATIONTYPE_CUSTOMER      11
#define APPLICATIONTYPE_BUSINESS      10


// For Twitter
#define kOAuthConsumerKey           @"8CDQjHRcv80RD2rM0IO2xQ"
#define kOAuthConsumerSecret        @"FgXI2Vt1xP1D5U46F44iRNkhjMFymv7hHK1MiHMhqOw"

// Facebook keys 

#define kApiKey                     @"250599871719740"
#define kApiSecret                  @"c129c5ddd70da2bb30835cc317ff8195"

#define BOTTOM_TAB_ITEM_COUNT_VM    3
 
#define REFRESH_HEADER_HEIGHT 70.0f
#define TABBARVIEW_HEIGHT 50
#define TABBARVIEW_ANIMATION_DURATION 0.50f
#define PROFILE_ANIMATION_DURATION 0.70f


#define REFRESH_HEADER_HEIGHT 70.0f
#define NAVBAR_HEIGHT 45
#define STATUSBAR_HEIGHT 22
//#define NAVIGATIONBAR_HEIGHT = ;


//http://www.danceworld-la.de

// FBConstant
#define FB_EXISTING              1
#define FB_CREATE                2

////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////// XML Tags /////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

#define TAG_ERROR_MESSAGE			@"message"
#define TAG_CODE					@"code"
#define TAG_SESSION					@"sessionid"
#define TAG_DATA					@"data"
#define TAG_USER					@"user"
#define TAG_UID						@"uid"
#define TAG_STATUS					@"status"
#define TAG_AVATAR					@"avatar"
#define TAG_VIEWCOUNT				@"viewcount"
#define TAG_JOMSOCIAL               @"jomsocial"
#define TAG_ACTIVITIES              @"activities"
#define TAG_ACTIVITY                @"activity"
#define TAG_REGISTRATION            @"registration"
#define TAG_TIMESTAMPDIFF           @"timestampDiff"


// CHATING
#define JS_DATA						@"l"
#define JS_USER_ID					@"i"
#define JS_USER_NAME				@"o"
#define JS_S						@"s"
#define JS_FROM						@"f"
#define JS_FROM_NAME				@"show"
#define JS_MESSAGE					@"m"
#define JS_LIST						@"lst"
#define JS_THUMB					@"thumb"
#define JS_LINK						@"link"


#define kUseLiveURL                 @"liveserver"
#define kLiveServerURL              @"liveurl"
#define kCurrentMonth               @"CurrentMonth"
#define kServerURL                  @"serverurl"
#define kExtraParam                 @"extraparam"
#define kUseDefaultURL              @"usedefault"

#define kThemeDefault                   @"Leather"

#define GENERAL_DATE_FORMAT				@"yyyy-MM-dd"
#define GENERAL_TIME_FORMAT				@"HH:mm:ss"
#define GENERAL_DATE_TIME_FORMAT_AMPM	@"yyyy-MM-dd HH:mm:ss"
#define GENERAL_COMMENT_DATE_FORMAT     @"MMM d, YYYY"
#define GENERAL_DATE_TIME_FORMAT_AMPM	@"yyyy-MM-dd HH:mm:ss"

/* Values for NSViewAlignment */
typedef NS_ENUM(NSInteger, NSViewAlignment) {
    NSViewHorizontalAlignmentLeft      = 0,    // Visually left aligned
    NSViewHorizontalAlignmentCenter    = 1,    // Visually centered
    NSViewHorizontalAlignmentRight     = 2,    // Visually right aligned
    NSViewHorizontalAlignmentJustified = 4,    // Fully-justified. The last line in a paragraph is
    NSViewHorizontalAlignmentNatural   = 5,    // Indicates the default alignment for script
    
    NSViewVerticalAlignmentTop      = 0,    // Visually left aligned
    NSViewVerticalAlignmentCenter    = 1,    // Visually centered
    NSViewVerticalAlignmentBottom     = 2,    // Visually right aligned
    NSViewVerticalAlignmentJustified = 4,    // Fully-justified. The last line in a paragraph is
    NSViewVerticalAlignmentNatural   = 5,    // Indicates the default alignment for script

} NS_ENUM_AVAILABLE_IOS(6_0);

typedef enum {
	
	jSuccess = 200,
	jNoContent = 204,
	jBadRequest = 400,
    jLoginRequired = 401,
	jErrorMessage = 404,
    jUnsupportedFile = 415,
    jInvalidData = 416,
    jErroronServer = 500,
    jUserNameError = 701,
    jEmailError = 702,
    jFBOption = 703,
    jSessionExpire = 704,
    jReportedContent = 705,
    JPermissionError = 706,
    jDuplicateData = 707,
    jWaitingForPermission = 708
    
} iJoomerErrorCode;


typedef enum {
	jLoginQuery = 0,
	jGeneralQuery,
	jProfileTypeQuery,
    jStatusQuery,
    jpostchatQuery,
	jRemoveUserQuery,
	jProfileQuery,
    jProjectDataQuery,
	jLogoutQuery,
	jFullProfileQuery,
	jInboxQuery,
	jOutboxQuery,
    jInnerMsgQuery,
	jFriendsQuery,
    jAlbumQuery,
    jAlbumAddQuery,
	jPhotoAlbumQuery,
    jPhotoAlbumImagesQuery,
    jAddphotoQuery,
    jPhotoCommentQuery,
	jVideoAlbumQuery,
	jVideoAlbumVideosQuery,
	jNotificationQuery,
	jAcceptQuery,
	jRejectQuery,
	jUpdatesQuery,
	jAddFriendQuery,
	jSettingsQuery,
	jAddCountQuery,
	jPollQuery,
	jPastPollQuery,
	jPollResultQuery,
	jWallListQuery,
	jLikeQuery,
	jUnlikeQuery,
    jDislikeQuery,
	jPostQuery,
	jShareQuery,
	jEditProfileQuery,
	jSearchQuery,
	jMembersQuery,
	jJReviewQuery,
	jArticlelistQuery,
	jArticleDetailQuery,
	jCollectionUploadQuery,
	jSubmitReviewQuery,
	jGroupQuery,
    jGroupSearchQuery,
	jGroupwallQuery,
	jBlogQuery,
	jBlogDetailQuery,
	jChatListQuery,
	jChatHeartbeat,
	jChatSendQuery,
	jEventListQuery,
	jEventSearchQuery,
	jRegisterQuery,
    jFBSignup,
    jTabListQuery,
    jCategoryQuery,
	jSubCategoryQuery,
    jTopicListQuery,
    jRecentTopicsQuery,
    jTopicDetailQuery,
	jFavoriteTopicsQuery,
	jSetFavQuery,
	jSetUnFavQuery,
	jSubscribedTopicsQuery,
	jSubscribeQuery,
	jUnsubscribeQuery,
	jWhoIsOnlineQuery,
	jNewTopicQuery,
	jMyTopicsQuery,
	jReplyListQuery,
	jAddReplyQuery,
	jCheckSubCategoryQuery,
	jSubscribeWallQuery,
	jUnsubscribeWallQuery,
	jJomEventCategoryListQuery,
	jJomMyEventListQuery,
    jJomEventMemberListQuery, 
	jJomPastEventListQuery,
	jJomEventDetailQuery,
	jJomPastInvitationListQuery,
	jJomEditEvent,
	jJomLikeQuery,
	jJomInvitedFriendListQuery,
	jJomWallListQuery,
	jJomUnBlockUserQuery,
	jJomShareQuery,
	jJomSpecialEventQuery,
	jJomSpecialGalleryAlbumListQuery,
	jJomSpecialGalleryQuery,
    jJoinQuery,
    jUnjoinQuery,
    jMygroupQuery,
    jPendingQuery,
    jGroupCatQuery,
    jBulletingQuery,
    jDiscussionQuery,
    jGroupDetailQuery,
    jInvitedFriendListQuery,
    jEditGroup,
    jAdminListQuery,
    jMemberQuery,
    jBanQuery,
    jJomEditGroup,
    jDiscussionDetailQuery,
    jPostGroupQuery,
    jGroupEventQuery,
    jAddVideoQuery,
    jAddArticle,
    
    //SOBI PRO
    
    Scategory,
	SPROcategory,
	Sentries,
	SPROentries,
	Sentries1,
	SPROentries1,
	Sdetails,
	SPROdetails,
	Sconfig,
	Search,
	Sreview,
	SearchSobi,
	SProSectionQuery,
	SPROSearchContent,
	SProconfig,
	SearchSobiPro,
    
    //Sobi2
    
    Sfeatured,
    
    //blog
    
    jBlogCategoryListQuery,
    jBlogTagListQuery,
    jBlogCommentListQuery,
    jMyBlogListQuery,
    jMyCommentListQuery, //blog's my comments
    jMyTagListQuery,
    jTagBlogListQuery,
    jAddNewBlogQuery,

    
    jvmCategoryQuery,
	jvmSubCategoryQuery,
	jvmProductQuery,
	jvmsubProductQuery,
	jvmReviewQuery,
	jvmProductDetail,
	jvmWishProductQuery,
	jvmProductListingQuery,
	jvmAddReviewQuery,
	jvmDuplicateEmail,
	jvmDuplicateUserName,
	jvmRegisterQuery,
	jvmAdReviewQuery,
	jvmLogoutQuery,
	jvmAddToWishListQuery,
	jvmWishListDetailQuery,
    jvmPostListRequestQuery,
    jvmCartListQuery,
    jvmFullRegisterQuery,
    jvmFeatureQuery,
    jvmBillingAddressQuery,
    jvmSaveRegisterQuery,
    jvmShippingMethodQuery,
    jvmPaymentMethodQuery,
    jvmConfirmOrderQuery,
    jvmCompleteOrderQuery,
    jvmOrderDetailQuery,
    jvmConfigQuery,
    jvmCouponQuery,
    jvmDeleteCartQuery,
    jvmMyAccountResponse,
    jvmSearchCatQuery,
    jvmSearchQuery,
    jvmWishListingQuery,
    jNewSplaceQuery,
    jRequestReset,
    jRequestConfirmReset,
    jRequestCompleteReset,
    jDeleteWallQuery,
    jlikeUnlikeQuery,
    
    //iCMS..
    iCMSCategoryListQuery,
    iCMSFeaturedArticleListQuery,
    iCMSArchivedArticleListQuery,
    iCMSArticleDetailQuery,
    ICMSPingTestQuery
    
    
    
} HTTPRequest;

////////////HeartDart TAGS

#define   QUERY_CREATE_TABLE @"CREATE TABLE IF NOT EXISTS"
#define   QUERY_INSERT_TABLE @"INSERT OR REPLACE INTO"
/////TABLE
#define TAG_ADVISOR          @"Advisor"

/////comman
#define TAG_ACCEPT                  @"accept"
#define TAG_REJECT                  @"reject"
#define TAG_ID                      @"id"
#define TAG_USERID                  @"userID"
#define TAG_ISPREMIUM               @"isPremium"
#define TAG_CAPTION                 @"caption"
#define TAG_IMAGES                  @"images"
#define TAG_IMAGEPATH               @"imagePath"
#define TAG_IMAGETHUMBNAIL          @"thumbnail"
#define TAG_IMAGEID                 @"imageID"


#define TAG_NAME                    @"name"
#define TAG_USERNAME                @"userName"
#define TAG_EMAIL                   @"email"
#define TAG_COUNTRYCODE             @"countryCode"
#define TAG_PHONE                   @"phone"
#define TAG_AVATAR                  @"avatar"
#define TAG_LOCATION                @"location"
#define TAG_LONGITUDE               @"longitude"
#define TAG_LATITUDE                @"latitude"



///////ADVISOR
#define TAG_ADVISORID              @"advisorID"
#define TAG_ADVISORUSERID          @"advisorUserID"
#define TAG_ADVISORNAME            @"advisorName"
#define TAG_ADVISOREMAIL           @"advisorEmail"
#define TAG_ISACTIVEADVISOR        @"isActiveAdvisor"
#define TAG_TIMESTAMP              @"timeStamp"
#define TAG_CREATED                @"created"
#define TAG_ADVISINGPEOPLE         @"advisingPeople"
#define TAG_HELPCOUNT              @"helpCount"
#define TAG_TOTALPPOST             @"totalPost"
#define TAG_YESVOTE                @"yesVote"
#define TAG_NOVOTE                 @"noVote"
#define TAG_ADVERT                 @"advertisement"


///////POST
#define TAG_VOTINGTIME             @"votingTime"
#define TAG_VOTES                  @"votes"
#define TAG_TOTALVOTES             @"totalVotes"
#define TAG_YESVOTECOUNT           @"yesVoteCount"
#define TAG_NOVOTECOUNT            @"noVoteCount"
#define TAG_TIMEREMAINING          @"remainingTime"
#define TAG_POSTUSERAVATAR         @"postUserAvatar"



// Post Table Field

#define POST_TABLE                  @"posts"

#define POST_ID                     @"postID"
#define POST_USER_ID                @"userID"
#define POST_CAPTION                @"caption"
#define POST_COMPARE                @"compare"
#define POST_VOTINGTIME             @"votingTime"
#define POST_TIMESTAMP              @"timeStamp"
#define POST_YES_VOTES              @"yesVoteCount"
#define POST_NO_VOTES               @"noVoteCount"
#define POST_USER_AVATAR            @"postUserAvatar"
#define POST_REMAINING_TIME         @"remainingTime"
#define POST_USER_NAME              @"userName"

// Votes table field

#define VOTE_TABLE                  @"votes"

#define VOTE_POST_ID                @"postID"
#define VOTE_ID                     @"voteID"
#define VOTE_USER_ID                @"userID"
#define VOTE_MESSAGE                @"message"
#define VOTE_TYPE                   @"voteType"
#define VOTE_LAT                    @"latitude"
#define VOTE_LONG                   @"longitude"
#define VOTE_LOCATION               @"location"
#define VOTE_PHONENO                @"phoneno"
#define VOTE_AVATAR                 @"avatar"
#define VOTE_NAME                   @"name"
#define VOTE_EMAIL                  @"email"
#define VOTE_USER_NAME              @"username"

// Images Table

#define IMAGES_TABLE                  @"images"

#define IMAGES_POST_ID                @"postID"
#define IMAGES_ID                     @"imageID"
#define IMAGES_PATH                @"imagePath"
#define IMAGES_YES_VOTE                @"yesVote"
#define IMAGES_NO_VOTE                   @"noVote"







