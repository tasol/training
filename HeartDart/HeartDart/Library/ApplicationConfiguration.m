//
//  ApplicationConfiguration.m
//  Intafy
//
//  Created by tasol on 2/11/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//



#import "ApplicationConfigurationDataProvider.h"

#import "DataBase.h"
@implementation ApplicationConfiguration
@synthesize modelNum;
@synthesize selectedNetworkID;
@synthesize selectedNetworkUserID;
@synthesize locationManager;
@synthesize appType;
@synthesize isLogedIn;
@synthesize restID;
@synthesize userID;
@synthesize deviceToken;
static ApplicationConfiguration *appConfig;

+ (ApplicationConfiguration*)sharedInstance{
    if (appConfig == nil) {
        appConfig = [[super allocWithZone:NULL] init];
//        [self intializeComponent];
        
        
    }

    return appConfig;
}
- (id)init{
    @try {
        self = [super init];
        if (self) {
            self.deviceToken=@"";
            self.selectedNetworkID=@"0";
            appType=0;
            isLogedIn=FALSE;
            restID=@"";
            userID=@"";
            deviceToken=@"";
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                self.POSTFIX_XIB=@"IPad";
            }else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
                if([[UIScreen mainScreen] bounds].size.height<568){
                    self.POSTFIX_XIB=@"Lower";
                }else{
                    self.POSTFIX_XIB=@"";
                }
            }
            self.isSocialStreamDownloading = FALSE;
            self.buttonBorderColor = [UIColor colorWithRed:90.0/255.0 green:200.0/255.0 blue:250.0/255.0 alpha:1.0];
            self.textColor = [UIColor colorWithRed:153.0/255.0 green:3.0/255.0 blue:4.0/255.0 alpha:1.0];
            self.descriptionFont=[UIFont fontWithName:@"Helvetica" size:12];
            
            [self performSelector:@selector(intializeComponent) withObject:nil afterDelay:0.1 ];
            [self createDatabase];
        }

    }
    @catch (NSException *exception)
    {
        
    }
    @finally {
        
    }
    return self;
}
-(void)createDatabase
{
    @try
    {
        NSArray *arrallkey=[[NSArray alloc]initWithObjects:TAG_ADVISORID,TAG_ADVISORNAME,TAG_ADVISOREMAIL,TAG_ISACTIVEADVISOR,TAG_AVATAR,TAG_LATITUDE,TAG_LONGITUDE,TAG_LOCATION,TAG_TOTALPPOST,TAG_HELPCOUNT,TAG_YESVOTE,TAG_NOVOTE,TAG_COUNTRYCODE,TAG_PHONE,TAG_ADVISINGPEOPLE, nil];
        NSString *Query = [[DataBase sharedInstance] Createtablestring:arrallkey];
        
        Query = [NSString stringWithFormat:@"%@ %@ (%@);",QUERY_CREATE_TABLE, [NSString stringWithFormat:@"%@%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"],TAG_ADVISOR], Query];
        NSLog(@"category QUERY %@",Query);
        [[DataBase sharedInstance] CreateTable:Query];
       
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(void)intializeComponent{
    @try {
        
        NSString *deviceName =[UIDevice currentDevice].model;
        NSLog(@"%@",deviceName);
//        if (![deviceName isEqualToString:@"iPhone Simulator"]) {

//            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
//            if ([userDefault valueForKey:@"ShowLocationFlag"] && [[userDefault valueForKey:@"ShowLocationFlag"] isEqualToString:@"0"]) {
//                [[ApplicationConfiguration sharedInstance].locationManager changeLocationUpdationStatus:FALSE];
//            }else if ([userDefault valueForKey:@"ShowLocationFlag"] && [[userDefault valueForKey:@"ShowLocationFlag"] isEqualToString:@"1"]){
            
//            }
            self.locationManager = [[GPSLocationListner alloc] init];
            [self.locationManager changeLocationUpdationStatus:TRUE];
//        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
-(UIFont*)getFontWithHeight:(int)size{

 return [UIFont fontWithName:@"Helvetica" size:size];
}
-(BOOL)fetchApplicationConfig{
    @try {

        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0) {
            if([[ UIScreen mainScreen ] bounds ].size.height == 568)
            {
                self.modelNum = 5;
            }
            else
            {
                self.modelNum = 4;
            }
        }
        else
        {
           self.modelNum = 3;
        }
        
        NSMutableDictionary *postVariables = [[NSMutableDictionary alloc] init];
        [postVariables setValue:@"iphone" forKey:@"device"];
        [postVariables setValue:[NSString stringWithFormat:@"%d", modelNum] forKey:@"type"];
        [[ApplicationConfigurationDataProvider sharedInstance] getApplicationConfigurationFromServerWithTaskData:postVariables];

        
    }
    @catch (NSException *exception) {
        DLog(@"Exception:%@",exception);
    }
    @finally {
        
    }
}
-(void)setModelNo:(int)no{
}
-(void)genarateServerURL{
    @try {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:kServerURL] length] > 0) {
            self.SERVER_URL =[[NSUserDefaults standardUserDefaults] objectForKey:kServerURL];
        }else{
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"ijoomerConfig.plist"]];
            self.SERVER_URL = [NSString stringWithFormat:@"%@/index.php?option=com_ijoomeradv",[plistDict objectForKey:@"sever_static_URL"]];
            
        }
    }
    @catch (NSException *exception) {
        DLog(@"Exception:%@",exception);
    }
    @finally {
        
    }
}
-(void)genarateServerURLWithNetworkId{
    @try {
        NSString *networkId = @"0";
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if([defaults valueForKey:@"selectedNetworkID"])
            networkId=[defaults valueForKey:@"selectedNetworkID"];
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:kServerURL] length] > 0) {
            self.SERVER_URL =[[NSUserDefaults standardUserDefaults] objectForKey:kServerURL];
        }else{
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"ijoomerConfig.plist"]];
            self.SERVER_URL = [NSString stringWithFormat:@"%@/index.php?option=com_ijoomeradv&network_id=%@",[plistDict objectForKey:@"sever_static_URL"],networkId];
        }
    }
    @catch (NSException *exception) {
        DLog(@"Exception:%@",exception);
    }
    @finally {
        
    }
}
-(void)genarateServerURLDynamic:(NSString*)URL{
    @try {
        [[NSUserDefaults standardUserDefaults] setObject:URL forKey:kServerURL];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    @catch (NSException *exception) {
        DLog(@"Exception:%@",exception);        
    }
    @finally {
    }
}

//-(void)processLogin{
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,0ul);
//    dispatch_async(queue, ^{
//        [[ApplicationConfiguration sharedInstance] genarateServerURLWithNetworkId];
//        NSDictionary *user=[[DreamyFlavourDataProvider sharedInstance] getUserByNetworkID:[ApplicationConfiguration sharedInstance].selectedNetworkID andUserID:[ApplicationConfiguration sharedInstance].selectedNetworkUserID];
//        NSMutableDictionary *postVariables = [[NSMutableDictionary alloc] init];
//        [postVariables setValue:[user valueForKey:@"userName"] forKey:@"username"];
//                [postVariables setValue:[user valueForKey:@"userId"] forKey:@"userid"];
//        [postVariables setValue:@""forKey:@"password"];
//        [postVariables setValue:[user valueForKey:@"email"] forKey:@"email"];
//        [postVariables setValue:self.deviceToken forKey:@"devicetoken"];
//        
//        [postVariables setValue:[NSString stringWithFormat:@"%f",[ApplicationConfiguration sharedInstance].locationManager.latitude] forKey:@"lat"];
//        [postVariables setValue:[NSString stringWithFormat:@"%f",[ApplicationConfiguration sharedInstance].locationManager.longitude] forKey:@"long"];
//        [postVariables setValue:@"iphone" forKey:@"type"];
//        BOOL sucess = [[DreamyFlavourDataProvider sharedInstance] loginWithTaskData:postVariables];
//        if (sucess) {
//
//            [[ApplicationConfiguration sharedInstance] genarateServerURLWithNetworkId];
//            postVariables = [[NSMutableDictionary alloc] init];
//            sucess = [[DreamyFlavourDataProvider sharedInstance] getProfile:postVariables withUserID:[ApplicationConfiguration sharedInstance].selectedNetworkUserID];
//            
//            
//        }
//        dispatch_sync(dispatch_get_main_queue(), ^{
//       
//        });
//    });
//}
@end
