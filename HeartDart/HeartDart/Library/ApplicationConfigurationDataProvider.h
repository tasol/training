//
//  ApplicationConfigurationDataProvider.h
//  Intafy
//
//  Created by tasol on 2/11/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "iJoomerDataProvider.h"

@interface ApplicationConfigurationDataProvider : iJoomerDataProvider
+ (ApplicationConfigurationDataProvider*)sharedInstance;
-(void)getApplicationConfigurationFromServerWithTaskData:(NSDictionary*)taskData;
-(BOOL)registerUserWithTaskData:(NSDictionary*)taskData;
@end
