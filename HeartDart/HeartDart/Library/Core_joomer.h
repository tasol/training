//
//  Core_joomer.h
//  Core_joomer
//
//  Created by Tailored Solutions on 11/6/12.
//  Copyright (c) 2013 tailored. All rights reserved.
//

#import <Foundation/Foundation.h>

#define sever_static_URL        @"http://192.168.5.156/development/ijoomer2.0/index.php?option=com_ijoomer"

#define JSON_MAIN           @"{\"task\":\"%@\",\"taskData\":%@}"

#define static_Logcashing_flag YES


@interface Core_joomer : NSObject


//Get dectionary from URL.
+ (NSDictionary *) sendRequest:(NSString *)jsonstring;

//Get dectionary from URL with image and Original.
+ (NSDictionary *) sendRequest:(NSString *)jsonstring Imaagedata :(NSData *)imagedata;

// To upload multiple images

+(NSDictionary * )sendRequest:(NSString *)jsonstring ImageArray:(NSArray *)imageArray;

//Get dectionary from URL with image.
+ (NSDictionary *) sendRequest:(NSString *)jsonstring Imaagedata :(NSData *)imagedata originalImageData:(NSData *)originalImageData;

//VideoUploading...
+ (NSDictionary *) dict_withVideo:(NSString *) jsonstring Videodata :(NSData *)Videodata static_URL :(NSString *)URL videoname:(NSString *) videoname;

// return Request time log values.
//+(NSArray *) RequestTimeLog;

//Voice Uploading....
+ (NSDictionary *) sendRequest:(NSString *) jsonstring Voicedata :(NSData *)voicedata;

//for voice and image
+ (NSDictionary *) dict_withVoice:(NSString *) jsonstring Imaagedata :(NSData *)imagedata Voicedata :(NSData *)voicedata static_URL:(NSString *)URL Voicename:(NSString *) voicename;
+ (NSDictionary *) sendRequestForSingleImagePost:(NSString *)jsonstring Imagedata :(NSData *)imagedata;

///***********************************Database coding*************************

//+(void) CheckDatabaseExist;
///*********************************End Database coding***********************


/////////////////////////////////////  jbolochat  ///////////////////////////////////
+ (NSDictionary *) dict_withimage:(NSString *) jsonstring Imaagedata :(NSData *)imagedata static_URL :(NSString *)URL fileType:(NSString *)fileType;



+(NSDictionary *) Reveel_dict_withVideo:(NSString *) jsonstring Videodata :(NSData *)Videodata  videoname:(NSString *) videoname videoImageData:(NSData *)videoImageData imageData:(NSData *)imageData;
@end
