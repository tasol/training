//
//  ErrorHangler.h
//  iJoomer
//
//  Created by tasol on 10/8/13.
//
//

#import <Foundation/Foundation.h>

@interface ErrorHandler : NSObject

+ (BOOL)hasError:(NSDictionary*)dict;
+(void)jBadRequest;
+(void)jErroronServer;
+(void)jErrorMessage;
+(void)jNoContent;
+(void)jUnsupportedFile;
+(void)jInvalidData;
+(void)jUserNameError;
+(void)jEmailError;
+(void)jFBOption;
+(void)jReportedContent;
+(void)JPermissionError;
+(void)jDuplicateData;
+(void)jWaitingForPermission;
+ (void)showAlert:(NSString *)title Content:(NSString *)bodyText;

@end
