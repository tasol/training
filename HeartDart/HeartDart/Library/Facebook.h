//
//  Facebook.h
//  FacebookExample
//
//  Created by Tasol on 3/10/14.
//  Copyright (c) 2014 Tailored. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>

typedef enum {
    FB_CALL_LOGIN,
    FB_CALL_FRIENDLIST,
    FB_CALL_TAGFRIENDLIST,
    FB_CALL_USERDATA
} FacebookCall;
@interface Facebook : NSObject
{
    FBSession *fbSession;
    NSArray *defaultPermissions;
    __block NSDictionary *dictUserData;
    FacebookCall facebookCall;
    
}
+ (Facebook*)sharedInstance;
@property (nonatomic,retain) FBSession *fbSession;
@property (nonatomic,retain) NSArray *defaultPermissions;

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
-(NSArray*)getFBData:(NSString*)userName data:(NSString*)dataWhichRequested;
/*
 This is dynamic method used to fetch data from facebook for a given username and with reuested data
 */

-(void)login; /* This method makes login for current user either via device fb login or askgin for login explicitly*/
-(void)logout;/*
               This method close and clear current session for this app
               */
-(FBSession*)openFBSession;/*
                            This method opens fbsession if previously available or will make call to login method..
                            */
-(void)closeFBSession;/*
                       This method closes the current fbsession for the app. It may open when needed by calling openFBSession method
                       */
-(void)setPermission:(NSArray*)permArr;/*
                                        This method adds permission into app for current user (additional permission like publish_action,user_likes,etc..)
                                        */
-(NSDictionary*)getuserData:(NSDictionary*)fields;/*
                                                   This method Retrieves user data from Facebbok for current logged in user.You can get (picture,id,birthday,email,name,gender,username,gender,first_name,last_name)
                                                   default permission required
                                                   */
-(BOOL)makeFBPostWithPhoto:(NSMutableDictionary*)param;/*
                                                        This method makes post on facebook wall with a message and a Photo
                                                        Permission : publish_actions
                                                        */
-(BOOL)addpermissionForPublish:(NSArray*)per;/*
                                              This method allows app for publishing content on facebook for current the user
                                              */
-(BOOL)changeStatus:(id)sender;/*
                                This method allows application to post new status on facebook wall for logged in user
                                Defaulr Permission needed
                                */
-(NSArray*)listFriends;/*
                        This method allows app to have all info of friends for the current user
                        Permission : user_friends
                        */
-(NSArray*)getUsersLikePages;/*
                              This method lets you to have info about pages which user liked
                              Permission : user_likes
                              */
-(NSArray*)getUserAccountInfo;/*
                               This method lets you info about pages which user owns(as admin)
                               permission : manage_pages
                               */
-(NSArray*)getUserActivity;/*
                            This method gives all info about user activities for current uesr
                            permission : user_activities
                            */
-(NSArray*)getuserAlbums;/*
                          This method lets you to get info about albums which user created
                          permission : user_photos
                          */
/* 11-3-14 */

-(NSArray*)getMutualFriends:(NSString*)friendID;/*
                                                 This method  allows you to fetch current user and provided friend id's mutual friends.
                                                 permission : default
                                                 */
-(NSArray*)getEventList;/*
                         This method gives you list of events of current logged in user
                         permission : user_events
                         */
-(NSArray*)getFamilyinfo;/*
                          This method gives you person's family relationship on facebook
                          permission : user_relationships
                          */
-(NSArray*)getuserGames;/*
                         This method gives you The list of games which you've mentioned in your sports inside facebook profile
                         permission : user_likes
                         */
-(NSArray*)getUserGroups;/*
                          This method gives you list of facebook groups of current user with "user_groups" permission
                          permission : user_groups
                          */
-(NSArray*)getuserHome;/*
                        The posts that a person sees in their Facebook News Feed.
                        permission : read_stream
                        */
-(NSArray*)getUserInboxMsg;/*
                            This method gives you list of facebook inbox msgs with its details for current user
                            permission : read_mailbox
                            */
-(NSArray*)getUserInterest;/*
                            This method gives you user interest which user mentioned in facebook profile
                            permission : user_interests
                            */
-(NSArray*)getUserLocation;/*
                            This method gives you user location in which user has been tagged
                            permission : user_photos or user_status
                            */
-(NSArray*)getUserLikedMovies;/*
                               The liked movies listed on current user's profile under Movies > Likes.
                               permission : user_likes
                               */
-(NSArray*)getuserLikedMusic; /*
                               The liked music artists listed on current user's profile under Music > Likes.
                               permission : user_likes
                               */
-(NSArray*)getuserNotes; /*
                          The Facebook Notes that a person has created
                          permission : user_notes
                          */
-(NSArray*)getUserNotification;/*
                                The unread Facebook notifications that a person has.
                                permission : manage_notifications
                                */
-(NSArray*)getUserOutboxMsg;/*
                             This method gives you list of facebook Outbox msgs with its details for current user
                             permission : read_mailbox
                             */
-(NSArray*)getAppPermission; /*
                              This method allows you to get permissions which user has given to app.
                              */
-(UIImage*)getUserProfilePic;/*
                              This method gives you user profile picture
                              */
-(NSArray*)getUserPokes;/*
                         The Facebook Pokes that a person has received and hasn't responded to.
                         permission : read_mailbox
                         */
-(NSArray*)getuserPosts;/*
                         This method gives list of posts made by the current user on facebook
                         permission : read_stream
                         */
-(NSArray*)getUserScores;/*
                          The scores this person has received from Facebook Games that they've played
                          permission : user_games_activity
                          */
// 12-3-14
-(NSArray*)getUserStatus;/*
                          This method shows only the status update posts that were published by this person.
                          permission : read_stream
                          */
-(NSArray*)getUserSubscribedto;/*
                                This method gives you the profile that this person is following.[following means  you'll see their posts in your News Feed ]
                                permission : user_subscriptions
                                */
-(NSArray*)getUserSubscribers;/*
                               This method gives you the profiles that are following this person 
                               permission : user_subscriptions
                               */
-(NSArray*)getUserLinks;/*
                         This method allows to fetch info about links those were publishe by current user
                         permission : user_likes
                         */
-(NSArray*)getFBPageInfo:(NSString*)pageId;/*
                                            This method gives you info about a particular facebook page
                                            permission : default
                                            */
//
-(BOOL)publishAlbum:(NSDictionary *)parameter;/*
                                               This method will add album in user account
                                               permission needed:
                                               perm1.publish_actions
                                               perm2.user_photos
                                               */
-(NSArray *)getUserPhotos:(NSString*)userID;/*This method will give the photos in the particular album.
                                             */

-(NSArray *)getUserTVShows;/*
                            This method will give the telivision shows which user has given to ther account
                            Permission needed:user_likes
                            */

-(NSArray *)photodownloaded;

-(NSArray*)listTagableFriends;

@end
