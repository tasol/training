//
//  Facebook.m
//  FacebookExample
//
//  Created by Tasol on 3/10/14.
//  Copyright (c) 2014 Tailored. All rights reserved.
//

#import "Facebook.h"

#import "AppConstantsList.h"
@implementation Facebook
@synthesize fbSession,defaultPermissions;

static Facebook *facebook=nil;
+ (Facebook*)sharedInstance
{
    if (facebook == nil)
    {
        facebook = [[super allocWithZone:NULL] init];
        
    }
    return facebook;
}

-(id)init
{
    @try
    {
        if(self = [super init]) {
            defaultPermissions = [[NSArray alloc] initWithObjects:@"email",@"user_friends", nil];
            fbSession=[[FBSession alloc]init];
//,@"taggable_friends"
        }
    
//        [LGOMDataProvider sharedInstance].facebookStateOpen=0;
//        [LGOMDataProvider sharedInstance].facebookStateClose=0;
//        [LGOMDataProvider sharedInstance].facebookStateCloseLoginFailed=0;

    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
    
        return self;
}
-(NSArray*)getFBData:(NSString*)userName data:(NSString*)dataWhichRequested
{
  
    @try{
    
    __block NSArray *resultData;
    
    [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"%@/%@",userName,dataWhichRequested]
        completionHandler:^(FBRequestConnection *connection , id result,NSError *error)
     {
         
       if(!error)
       {
           resultData=(NSArray*)result;
           NSLog(@"Resulted Data : %@",resultData);
       }
       else
           [self showAlertView:@"Could not retrieve requested data" message:error.description];
     }
     ];
    return resultData;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getUserTVShows
{
    __block NSArray *tvShows;
    @try {
        
        [FBRequestConnection startWithGraphPath:@"me/television" parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection,id result,NSError *error){
            if (error) {
                NSLog(@"Error in television %@",error.description);
            }
            else {
                tvShows=[result objectForKey:@"data"];
                NSLog(@"FAviorate Tvshows are: %@",tvShows);
            }
        }];
        return tvShows;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception : %@",exception);
    }
    @finally {
        
    }
}
-(BOOL)publishAlbum:(NSDictionary *)parameter
{
    __block BOOL flag=NO;
    @try {
        [FBRequestConnection startWithGraphPath:@"/me/albums" parameters:parameter HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection,id result,NSError *error){
            if (error) {
                NSLog(@"Error in publishing %@",error.description);
                flag=NO;
            }
            else{
                NSLog(@"success :%@",result);
                NSLog(@"Published successfully");
                flag=YES;
            }
        }];
        return flag;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception handled");
    }
    @finally {
        
    }
}
-(NSArray*)getUserPhotos:(NSString *)userID
{
    
   __block NSArray *photos;
    @try {
        //NSString *str=@"117913238293298";
        NSString * path = [NSString stringWithFormat:@"%@/photos", userID];
        [FBRequestConnection startWithGraphPath:path parameters:nil HTTPMethod:@"GET" completionHandler:^
         (FBRequestConnection *connection,id result,NSError *error){
             if (!error) {
                 photos=(NSArray*)result;
                 NSLog(@"array is :%@",photos);
                 
             }
         }];
        
        return photos;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception found : %@",exception);
    }
    @finally {
        
    }
   
}
-(void)login
{
    @try {
    
    if(![self openSessionWithAllowLoginUI:NO])
    {
        [self openSessionWithAllowLoginUI:YES];
    }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    
    @try {

    BOOL flag=[FBSession openActiveSessionWithReadPermissions:self.defaultPermissions
                                       allowLoginUI:allowLoginUI
                                  completionHandler:^(FBSession *session,
                                                      FBSessionState state,
                                                      NSError *error) {
                                      self.fbSession=session;
                                      [self sessionStateChanged:session
                                                          state:state
                                                          error:error];
                                  }];
    return flag;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getUsersLikePages
{
    @try{
    __block NSArray *likedPages;
    [FBRequestConnection startWithGraphPath:@"me/likes" completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
    {
        if(!error)
        {
            NSLog(@"user like");
            likedPages=(NSArray*)result;
             NSLog(@"%@",likedPages);
            
        }
        else
        {
            [self showAlertView:@"could not get User likes" message:error.description];
        }
    } ];
    return likedPages;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getUserAccountInfo
{
    @try{
    __block NSArray *accInfo;
    [FBRequestConnection startWithGraphPath:@"/me/accounts"
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(
                                              FBRequestConnection *connection,
                                              id result,
                                              NSError *error
                                              ) {
                              if(!error)
                              {
                                  
                                  accInfo=(NSArray*)result;
                                  NSLog(@"result : %@",accInfo);
                              }
                              else
                                  [self showAlertView:@"could not access A/c info" message:error.description];
                              /* handle the result */
                          }];
    
    return accInfo;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getUserActivity
{
    
    @try{
    __block NSArray *activites;
    [FBRequestConnection startWithGraphPath:@"/me/activities"
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(
                                              FBRequestConnection *connection,
                                              id result,
                                              NSError *error
                                              ) {
                              if(!error)
                              {
                                  
                                  activites=(NSArray*)result;
                                  NSLog(@"result : %@",activites);
                              }
                              else
                                  [self showAlertView:@"could not access A/c info" message:error.description];
                              /* handle the result */
                          }];
    
    return activites;

    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getuserAlbums
{
    @try{
    __block NSArray *albums;
    [FBRequestConnection startWithGraphPath:@"/me/albums"
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(
                                              FBRequestConnection *connection,
                                              id result,
                                              NSError *error
                                              ) {
                              if(!error)
                              {
                                  
                                  albums=(NSArray*)result;
                                  NSLog(@"result : %@",albums);
                              }
                              else
                                  [self showAlertView:@"could not access A/c info" message:error.description];
                              /* handle the result */
                          }];
    
    return albums;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }

}
- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    
    @try{
    switch (state) {
        case FBSessionStateOpen:
            if (!error)
            {
                // We have a valid session
                 NSLog(@"User session open successfully");
//                [LGOMDataProvider sharedInstance].facebookStateOpen=1;
                // NSLog(@"session %@",session);

                if (!dictUserData && facebookCall == FB_CALL_USERDATA)
                {
                    [FBRequestConnection startWithGraphPath:@"me" parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
                     {
                         if(error)
                         {
                             NSLog(@"Error : %@",error);
                         }
                         else
                         {
                             
                             dictUserData =(NSDictionary*)result;
                             NSLog(@"Data  : %@",dictUserData);
                         }
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDataDownload" object:dictUserData];
                     }
                     ];
                }else if (facebookCall == FB_CALL_FRIENDLIST){
                    [self listFriends];
                    
                }else if (facebookCall == FB_CALL_TAGFRIENDLIST){
                    [self listTagableFriends];
                }
            }
            break;
        case FBSessionStateClosed:
        {
//            [LGOMDataProvider sharedInstance].facebookStateClose=1;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDeniesFacebookShare" object:nil];
            NSLog(@"session closed");
        }
            break;
        case FBSessionStateClosedLoginFailed:
        {
//            [LGOMDataProvider sharedInstance].facebookStateCloseLoginFailed=1;
            [FBSession.activeSession closeAndClearTokenInformation];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDeniesFacebookShare" object:nil];
            
            NSLog(@"state closed and loginfailed");
        }
            break;
        default:
            break;
    }
    if (error)
    {
        
        NSString *valueError = [error.userInfo objectForKey:FBErrorLoginFailedReason];
        if ([valueError compare:FBErrorLoginFailedReasonSystemDisallowedWithoutErrorValue] == NSOrderedSame)
        {
            NSString *msg=@"To use your Facebook account with this app, open Settings > Facebook and make sure this app is turned on.";
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Error"
                                      message:msg
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDeniesFacebookShare" object:nil];
        }
    }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(void)logout
{
    @try{
    NSLog(@"you have successfully loggedout");
    [self.fbSession closeAndClearTokenInformation];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(FBSession*)openFBSession
{
    @try{
    [self login];
    NSLog(@"session opened");
    return self.fbSession;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(void)closeFBSession
{
    @try{
    [self.fbSession close];
    NSLog(@"Session closed....");
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(void)setPermission:(NSArray*)permArr
{
    @try{
//        [self.defaultPermissions addObjectsFromArray:permArr];
        [self login];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSDictionary*)getuserData:(NSDictionary*)fields
{
    @try{
        if (![fbSession isOpen]) {
            facebookCall = FB_CALL_USERDATA;
            [self login];

        }else {
    
        [FBRequestConnection startWithGraphPath:@"me" parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
            {
                if(error)
                {
                    NSLog(@"Error : %@",error);
                }
                else
                {
                  
                  dictUserData =(NSDictionary*)result;
                      NSLog(@"Data  : %@",dictUserData);
                }
//                 NSArray *permission=[[NSArray alloc]initWithObjects:@"publish_actions", nil];
//                [self addpermissionForPublish:permission];
                
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDataDownload" object:dictUserData];
            }
         ];
     }
    return dictUserData;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}

-(void)showAlertView:(NSString*)title message:(NSString*)message
{
    @try{
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:title
                              message:message
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
    [alertView show];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(BOOL)changeStatus:(NSString*)staus
{
    @try{
    
    __block BOOL flag=NO;
    [FBRequestConnection startForPostStatusUpdate:staus completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
    {
        if(!error){
            NSLog(@"Status updated successfully");
            flag=YES;
        }
        else
        {
            [self showAlertView:@"Could not update status" message:error.description];
            flag=NO;
        }
    } ];
    return flag;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getMutualFriends:(NSString*)friendID
{
    @try{
    __block NSArray *friends;
    
    NSString *path=[NSString stringWithFormat:@"me/mutualfriends/%@",friendID];
    
    [FBRequestConnection startWithGraphPath:path
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(
                                              FBRequestConnection *connection,
                                              id result,
                                              NSError *error
                                              ) {
                              if(!error)
                              {
                                  friends=(NSArray*)result;
                                  NSLog(@"Friends : %@",result);
                              }
                              else
                                  [self showAlertView:@"could not find mutual friends" message:error.description];
                              /* handle the result */
                          }];
    return friends;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(BOOL)makeFBPostWithPhoto:(NSMutableDictionary*)param
{
    @try{
    __block BOOL flag=NO;
    [FBRequestConnection startWithGraphPath:@"me/feed"
                                 parameters:param
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              
                              if(!error)
                              {
                                  NSLog(@"result : %@",result);
                                  NSLog(@"Posted successfully");
                                  flag=YES;
                                  
                              }
                              else
                              {
                                  [self showAlertView:@"Error" message:error.description];
                                  flag=NO;
                              }
                              
                          }];
    return flag;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getEventList
{
    @try{
    __block NSArray *eventList;
    
    [FBRequestConnection startWithGraphPath:@"/me/events"
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(
                          FBRequestConnection *connection,
                          id result,
                          NSError *error
                          )
                          {
                              if(!error)
                              {
                                  eventList=(NSArray*)result;
                                  NSLog(@"result : %@",result);
                              }
                              else
                                  [self showAlertView:@"Could not find Event List" message:error.description];
                          }];
    return eventList;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getFamilyinfo
{
    @try{
    __block NSArray *familyInfo;
    [FBRequestConnection startWithGraphPath:@"/me/family"
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(
                          FBRequestConnection *connection,
                          id result,
                          NSError *error
                          )
                          {
                              if(!error)
                              {
                                  NSLog(@"Familyinfo :%@",result);
                                  familyInfo=(NSArray*)result;
                                  NSLog(@"Familyinfo :%@",familyInfo);
                              }
                          }
    ];
    return familyInfo;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getUserGroups
{
    @try{
    __block NSArray *groups;
    [FBRequestConnection startWithGraphPath:@"me/groups" completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
     {
         if(!error)
         {
             NSLog(@"user like");
             groups=(NSArray*)result;
             NSLog(@"%@",groups);
             
         }
         else
         {
             [self showAlertView:@"could not get User likes" message:error.description];
         }
     } ];
    return groups;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getuserHome
{
    @try{ __block NSArray *groups;
    [FBRequestConnection startWithGraphPath:@"me/home" completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
     {
         if(!error)
         {
             NSLog(@"Home Info");
             groups=(NSArray*)result;
             NSLog(@"%@",groups);
             
         }
         else
         {
             [self showAlertView:@"could not get User likes" message:error.description];
         }
     } ];
    return groups;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getuserGames
{
    @try{
    __block NSArray *homeInfo;
    [FBRequestConnection startWithGraphPath:@"me/home" completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
     {
         if(!error)
         {
             NSLog(@"USer games");
             homeInfo=(NSArray*)result;
             NSLog(@"%@",homeInfo);
             
         }
         else
         {
             [self showAlertView:@"could not get User likes" message:error.description];
         }
     } ];
    return homeInfo;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getUserInterest
{
    @try{
    __block NSArray *interest;
    [FBRequestConnection startWithGraphPath:@"me/interests" completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
     {
         if(!error)
         {
             NSLog(@"USer Interest : %@",result);
             interest=(NSArray*)result;
             NSLog(@"%@",interest);
             
         }
         else
         {
             [self showAlertView:@"could not get User interest" message:error.description];
         }
     } ];
    return interest;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getUserLocation
{
    @try{
    __block NSArray *location;
    [FBRequestConnection startWithGraphPath:@"me/locations" completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
     {
         if(!error)
         {
             NSLog(@"USer Location : %@",result);
             location=(NSArray*)result;
             NSLog(@"%@",location);
             
         }
         else
         {
             [self showAlertView:@"could not get User location Info" message:error.description];
         }
     } ];
    return location;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getUserLikedMovies
{
    @try{
    __block NSArray *moviesInfo;
    [FBRequestConnection startWithGraphPath:@"me/movies" completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
     {
         if(!error)
         {
             NSLog(@"USer movies : %@",result);
             moviesInfo=(NSArray*)result;
             NSLog(@"%@",moviesInfo);
             
         }
         else
         {
             [self showAlertView:@"could not get User liked movies list" message:error.description];
         }
     } ];
    return moviesInfo;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getuserNotes
{
    @try{
    __block NSArray *notesInfo;
    [FBRequestConnection startWithGraphPath:@"me/notes" completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
     {
         if(!error)
         {
             NSLog(@"USer notes : %@",result);
             notesInfo=(NSArray*)result;
             NSLog(@"%@",notesInfo);
             
         }
         else
         {
             [self showAlertView:@"could not get User notes" message:error.description];
         }
     } ];
    return notesInfo;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
    
}
-(NSArray*)getuserLikedMusic
{
    @try{
    __block NSArray *musicInfo;
    [FBRequestConnection startWithGraphPath:@"me/music" completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
     {
         if(!error)
         {
             NSLog(@"USer music : %@",result);
             musicInfo=(NSArray*)result;
             NSLog(@"%@",musicInfo);
             
         }
         else
         {
             [self showAlertView:@"could not get User liked music list" message:error.description];
         }
     } ];
    return musicInfo;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
    
}
-(NSArray*)getUserNotification
{
    @try{
    __block NSArray *notifInfo;
    [FBRequestConnection startWithGraphPath:@"me/notifications" completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
     {
         if(!error)
         {
             NSLog(@"USer notifications : %@",result);
             notifInfo=(NSArray*)result;
             NSLog(@"%@",notifInfo);
             
         }
         else
         {
             [self showAlertView:@"could not get User notifications info" message:error.description];
         }
     } ];
    return notifInfo;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
    
}
-(NSArray*)getAppPermission
{
    @try{
    __block NSArray *permissions;
    [FBRequestConnection startWithGraphPath:@"me/permissions" completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
     {
         if(!error)
         {
//             NSLog(@"USer outbox Message Info");
             permissions=(NSArray*)result;
             NSLog(@"%@",permissions);
             
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UserAppPermission" object:permissions];
         }
         else
         {
             [self showAlertView:@"could not get User message outbox" message:error.description];
         }
     } ];
    return permissions;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getuserPosts
{
    __block NSArray *postsInfo;
    [FBRequestConnection startWithGraphPath:@"me/posts" completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
     {
         if(!error)
         {
             NSLog(@"USer Posts Info");
             postsInfo=(NSArray*)result;
             NSLog(@"%@",postsInfo);
             
         }
         else
         {
             [self showAlertView:@"could not get User posts info" message:error.description];
         }
     } ];
    return postsInfo;
}
-(NSArray*)getUserSubscribedto
{
    @try{
    __block NSArray *subscribedTo;
    [FBRequestConnection startWithGraphPath:@"/me/subscribedto"
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(
                          FBRequestConnection *connection,
                          id result,
                          NSError *error
                          )
                        {
                            if(!error)
                            {
                                 NSLog(@"result : %@",result);
                                 subscribedTo=(NSArray*)result;
                                 NSLog(@"Data Array of subscription : %@",subscribedTo);
                            }
                            else
                                [self showAlertView:@"user subscription info could not found" message:error.description];
                        }
     ];
    return subscribedTo;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getUserSubscribers
{
    @try{
    __block NSArray *subscribers;
    [FBRequestConnection startWithGraphPath:@"/me/subscribers"
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(
                          FBRequestConnection *connection,
                          id result,
                          NSError *error
                          )
                        {
                         if(!error)
                         {
                             //NSLog(@"result : %@",result);
                             subscribers=(NSArray*)result;
                             NSLog(@"Data Array of subscribed users : %@",subscribers);
                         }
                         else
                             [self showAlertView:@"user subscribed info could not found" message:error.description];
                     }
     ];
    return subscribers;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}

-(NSArray*)getUserLinks
{
    @try{
    __block NSArray *linksInfo;
    [FBRequestConnection startWithGraphPath:@"me/links" completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
     {
         if(!error)
         {
             //NSLog(@"USer Links Info");
             linksInfo=(NSArray*)result;
             NSLog(@"Links Info : %@",linksInfo);
             
         }
         else
         {
             [self showAlertView:@"could not get User posts info" message:error.description];
         }
     } ];
    return linksInfo;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getFBPageInfo:(NSString*)pageId
{
    @try{
    __block NSArray *pageInfo;
    [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"%@",pageId] completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
     {
         if(!error)
         {
             //NSLog(@"USer Links Info");
             pageInfo=(NSArray*)result;
             NSLog(@"Page Info : %@",pageInfo);
             
         }
         else
         {
             [self showAlertView:@"could not get User posts info" message:error.description];
         }
     } ];
    return pageInfo;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getUserStatus
{
    @try{
    __block NSArray *statusInfo;
    [FBRequestConnection startWithGraphPath:@"/me/statuses"
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(
                          FBRequestConnection *connection,
                          id result,
                          NSError *error
                          )
                          {
                              if(!error)
                              {
                                  NSLog(@"result : %@",result);
                                  statusInfo=(NSArray*)result;
                                  NSLog(@"Data Array : %@",statusInfo);
                              }
                              else
                                  [self showAlertView:@"Status info could not found" message:error.description];
                          }
     ];
    return statusInfo;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getUserScores
{
    @try{
    __block NSArray *scoresInfo;
    [FBRequestConnection startWithGraphPath:@"me/scores" completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
     {
         if(!error)
         {
             NSLog(@"USer Scores Info");
             scoresInfo=(NSArray*)result;
             NSLog(@"%@",scoresInfo);
             
         }
         else
         {
             [self showAlertView:@"could not get User scores info" message:error.description];
         }
     } ];
    return scoresInfo;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getUserPokes
{
    @try{
    __block NSArray *pokesInfo;
    [FBRequestConnection startWithGraphPath:@"me/pokes" completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
     {
         if(!error)
         {
             NSLog(@"USer pokes Info");
             pokesInfo=(NSArray*)result;
             NSLog(@"%@",pokesInfo);
             
         }
         else
         {
             [self showAlertView:@"could not get User pokes info" message:error.description];
         }
     } ];
    return pokesInfo;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(UIImage*)getUserProfilePic
{
    @try{
    __block UIImage *profileImage;
    [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *FBuser, NSError *error)
        {
        if (error) {
            // Handle error
            NSLog(@"error : %@",error);
        }
        
        else
        {
            NSString *username = [FBuser name];
            NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", username];
            NSLog(@"Image URL :%@",userImageURL);
            NSURL *url=[[NSURL alloc]initWithString:userImageURL];
            NSData *data=[[NSData alloc] initWithContentsOfURL:url];
            profileImage=[[UIImage alloc]initWithData:data];
        }
    }];
    return profileImage;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(NSArray*)getUserOutboxMsg
{
    @try{
    __block NSArray *msgs;
    [FBRequestConnection startWithGraphPath:@"me/outbox" completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
     {
         if(!error)
         {
             NSLog(@"USer outbox Message Info");
             msgs=(NSArray*)result;
             NSLog(@"%@",msgs);
             
         }
         else
         {
             [self showAlertView:@"could not get User message outbox" message:error.description];
         }
     } ];
    return msgs;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
    
}
-(NSArray*)getUserInboxMsg
{
    @try{
    __block NSArray *msgs;
    [FBRequestConnection startWithGraphPath:@"me/inbox" completionHandler:
     ^(FBRequestConnection *connection,id result,NSError *error)
     {
         if(!error)
         {
             NSLog(@"USer Inbox Message Info");
             msgs=(NSArray*)result;
             NSLog(@"%@",msgs);
             
         }
         else
         {
             [self showAlertView:@"could not get User message inbox" message:error.description];
         }
     } ];
    return msgs;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
    
}
-(NSArray*)listTagableFriends
{
    @try{
        
        if (![facebook.fbSession isOpen])
        {
            facebookCall = FB_CALL_TAGFRIENDLIST;
            [facebook login];
            return nil;
        }
        __block NSArray *friends;
        
        [FBRequestConnection startWithGraphPath:@"/me/taggable_friends"
                                     parameters:nil
                                     HTTPMethod:@"GET"
                              completionHandler:^(
                                                  FBRequestConnection *connection,
                                                  id result,
                                                  NSError *error
                                                  ) {
                                  if (error) {
                                      
                                      [self showAlertView:@"Could not find friend list" message:error.description];
                                  }
                                  else{
                                      friends=(NSArray*)result;
                                      NSLog(@"Friends Info : %@",friends);
                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"TaggableFriendListDoanloaded" object:friends];
                                  }
                                  
                              }];
        
        
        
        return friends;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}

-(NSArray*)listFriends
{
    @try{
        
        if (![facebook.fbSession isOpen])
        {
            facebookCall = FB_CALL_FRIENDLIST;
            [facebook login];
            return nil;
        }
    __block NSArray *friends;
    
        [FBRequestConnection startWithGraphPath:@"/me/friends"
                                     parameters:nil
                                     HTTPMethod:@"GET"
                              completionHandler:^(
                                                  FBRequestConnection *connection,
                                                  id result,
                                                  NSError *error
                                                  ) {
                                  if (error) {
                                      
                                      [self showAlertView:@"Could not find friend list" message:error.description];
                                  }
                                  else{
                                      friends=(NSArray*)result;
                                      NSLog(@"Friends Info : %@",friends);
                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"FriendListDoanloaded" object:friends];
                                  }
                                  
                              }];
        
        
        
        return friends;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}
-(BOOL)addpermissionForPublish:(NSArray*)permission
{
    @try{
    __block BOOL flag=NO;
    [self.fbSession requestNewPublishPermissions:permission defaultAudience:FBSessionDefaultAudienceFriends completionHandler:
     ^(FBSession *session,NSError *error)
    {
        if(!error)
        {
            
            NSLog(@"Permission added successfully");
            flag=YES;
        }
        else
        {
//            [self showAlertView:@"could not add permission" message:@"Please allow app to user Facebbok"];
           [[NSNotificationCenter defaultCenter] postNotificationName:@"FBLoginDenied" object:nil];
            
        }
    } ];
    return flag;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
}

-(NSArray *)photodownloaded{
    @try {
        __block NSArray *photos;
    
    [FBRequestConnection startWithGraphPath:@"me/photos" parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection,id result,NSError *error){
        if (!error) {
            photos=(NSArray *)result;
            NSLog(@"Photos are %@",photos);
        }
    }];
    return photos;
    }
    @catch (NSException *exception) {
        NSLog(@"E %@",exception);
    }
    @finally {
        
    }
}

@end
