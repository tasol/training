//
//  CGRectMake.h
//  iJoomer
//
//  Created by tailored on 10/26/13.
//
//

#import <Foundation/Foundation.h>
#import "AppConstantsList.h"

@interface ijoomerCGrect : NSObject

UIKIT_STATIC_INLINE CGRect ijoomerCGrect1(CGFloat xx,CGFloat yy,CGFloat widthh,CGFloat heighth) {
    
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        
        double xxx = ((xx*768)/320);
        double yyy = ((yy*1024)/480);
        double widthth = ((widthh*768)/320);
        double heightt = ((heighth*1004)/480);
        CGRect iPadRect = CGRectMake(xxx, yyy, widthth, heightt);
       
        if(xxx > 768 || yyy >1004 || widthth > 768 || heightt>1004)
           iPadRect = CGRectMake(xx,yy,widthh, heighth);

            
        return iPadRect;
    }
    else
    {
        if([[ UIScreen mainScreen ] bounds ].size.height!=568){
            double scale =([[ UIScreen mainScreen ] bounds ].size.height/568);
            xx=(xx*scale);
            yy=(yy*scale);
            widthh = (widthh*scale);
            heighth = (heighth*scale);

            
        }
        return CGRectMake(xx, yy, widthh, heighth);
    }
    
}

UIKIT_STATIC_INLINE void setFrame(UIView *view,NSViewAlignment alignmentHorizontal,NSViewAlignment alignmentVerticle,BOOL scaleWidth,BOOL scaleHeight){
    
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        
        double xxx = ((view.frame.origin.x*768)/320);
        double yyy = ((view.frame.origin.y*1024)/480);
        double widthth = ((view.frame.size.width*768)/320);
        double heightt = ((view.frame.size.height*1004)/480);
        CGRect iPadRect = CGRectMake(xxx, yyy, widthth, heightt);
        
        if(xxx > 768 || yyy >1004 || widthth > 768 || heightt>1004)
            iPadRect = CGRectMake(view.frame.origin.x,view.frame.origin.y,view.frame.size.width, view.frame.size.height);
        view.frame=iPadRect;
    }
    else
    {

        double width = view.frame.size.width;
        double height = view.frame.size.height;
        double x = view.frame.origin.x;
        double y = view.frame.origin.y;
        if([[ UIScreen mainScreen ] bounds ].size.height!=568){

            double scale =([[ UIScreen mainScreen ] bounds ].size.height/568);
            if([view isKindOfClass:[UIButton class]]||[view isKindOfClass:[UIView class]]){
                if(scaleWidth){
                    width = (width*scale);
                }
                if(scaleHeight){
                    height = (height*scale);
                }
            }
            if(alignmentHorizontal==NSViewHorizontalAlignmentLeft){
                x =(x*scale);
            }else if(alignmentHorizontal==NSViewHorizontalAlignmentRight){
            }if(alignmentHorizontal==NSViewHorizontalAlignmentCenter){
                x = (view.superview.frame.size.width - view.frame.size.width)/2;
            }
            
            
            if(alignmentVerticle==NSViewVerticalAlignmentTop){
                y =(y*scale);
            }else if(alignmentVerticle==NSViewVerticalAlignmentBottom){
            }if(alignmentVerticle==NSViewVerticalAlignmentCenter){
                y = (view.superview.frame.size.height - view.frame.size.height)/2;
            }
            
            view.frame=CGRectMake(x, y, width, height);
        }
        
    }
    
}
@end
