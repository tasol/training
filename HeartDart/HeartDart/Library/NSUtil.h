//
//  NSUtil.h
//  Intafy
//
//  Created by tasol on 2/11/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUtil : NSObject

+(NSString*)getJsonStringFromDictionary:(NSDictionary*)dictionary;
+(void)getImageForView:(UIView*)imageView fromURL:(NSString *)imgUrl alterText:(NSString*)alterText;
+(void)getImageForView1:(UIView*)imageView fromURL:(NSString *)imgUrl alterText:(NSString*)alterText;
+(UIImage*)getImageFromURL:(NSString *)imgUrl;
+(NSArray*)shortDirectoryArray:(NSArray*)arrayToShort ByKey:(NSString*)key;
+(NSString*)convertDate:(NSString*)dateToConver;
+(void)applyCircleAsBorder:(UIButton*)view;
+(NSString*)convertDate:(NSString*)dateToConver dateFormate:(NSString*)dateFormate dateFormateToConvert:(NSString*)dateFormateToConvert;
+(NSString*)getDataFromURL:(NSString *)imgUrl;
+ (UIImage *) cropImage:(UIImage *)originalImage;
+(UIImage*)cropImageFromMiddle:(UIImage *)originalImage;
+(void)cropImageFromMiddleVertically:(UIImage *)originalImage forView:(UIImageView *)imageView;
+(NSString*)convertDateTimeStamp:(NSString*)timestamp DateFormat:(NSString*)dateFormat;
+(long)getUTCTimeStamp;
+(NSData*)getImageDataFromURL:(NSString *)imgUrl;
+(UIImage *)scaleAndRotateImage :(UIImage*)image;
+(UIColor*)colorWithHexString:(NSString*)hex;
@end

@interface NSDictionary (JSONKitSerializing)
- (NSString *)JSONString;

@end
@interface NSArray (JSONKitSerializing)
- (NSString *)JSONString;

@end
