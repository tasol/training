//
//  ProgressBar.h
//  LGOM
//
//  Created by Kalpesh on 9/15/14.
//  Copyright (c) 2014 LGOM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressBar : UIView
{
    CGFloat startAngle;
    CGFloat endAngle;
    long timeRemaining;
    NSString *strTime;
    NSString *strVote;
}
@property(nonatomic,assign)int totalInvitation;
@property(nonatomic,assign)int collectedVote;
@property(nonatomic,assign)float percent;
@property(nonatomic,assign)long timeRemaining;

@property(nonatomic,retain)NSString *strTime;
@property(nonatomic,retain)NSString *strVote;

@end
