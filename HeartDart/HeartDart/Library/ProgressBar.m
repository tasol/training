//
//  ProgressBar.m
//  LGOM
//
//  Created by Kalpesh on 9/15/14.
//  Copyright (c) 2014 LGOM. All rights reserved.
//

#import "ProgressBar.h"

@implementation ProgressBar
@synthesize timeRemaining,percent;
@synthesize strTime,strVote;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
        
        // Determine our start and stop angles for the arc (in radians)
        startAngle = M_PI * 0;
        endAngle = startAngle + (M_PI * 2);
        self.percent=0;
        self.strTime=@"";
        self.strVote=@"";
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
 
    
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    
    // Text Drawing for time
    CGRect textRect1 = CGRectMake(0, 60, 200, 20);
    [[ApplicationConfiguration sharedInstance].textColor setFill];
    [self.strTime drawInRect: textRect1 withFont: [UIFont fontWithName: @"Helvetica-Bold" size: 20] lineBreakMode: NSLineBreakByWordWrapping alignment: NSTextAlignmentCenter];

    
    // Text Drawing for vore
    CGRect textRect = CGRectMake(0, 119, 200, 20);
    [[UIColor whiteColor] setFill];
    [self.strVote drawInRect: textRect withFont: [UIFont fontWithName: @"Helvetica-Bold" size: 20] lineBreakMode: NSLineBreakByWordWrapping alignment: NSTextAlignmentCenter];

    
    
    
    
    
    // Create our arc, with the correct angles
    [bezierPath addArcWithCenter:CGPointMake(rect.size.width / 2, rect.size.height / 2)
                          radius:((rect.size.width / 2)-10)
                      startAngle: DEGREES_TO_RADIANS(-90)
                        endAngle:DEGREES_TO_RADIANS(percent-90)
                       clockwise:YES];
    
    // Set the display for the path, and stroke it
    bezierPath.lineWidth = 15;
    [[ApplicationConfiguration sharedInstance].textColor setStroke];
    [bezierPath stroke];
}
@end
