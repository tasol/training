//
//  TableSection.h
//  iJoomer
//
//  Created by tasol on 1/8/14.
//
//

#import <Foundation/Foundation.h>
#import "TableRowObject.h"
#import "TableRowObject.h"


@interface TableSection : NSObject
{
    NSString *name;
    NSMutableArray *tableObjectArray;
    NSMutableArray *shortedArray;

}
@property(nonatomic,retain)NSString *name;
@property(nonatomic,retain)NSMutableArray *tableObjectArray;
@property(nonatomic,retain)NSMutableArray *shortedArray;
-(void)addObject:(TableRowObject*)object;
-(NSUInteger)getCount;
-(TableRowObject*)objectAtIndex:(int)index;

@end
